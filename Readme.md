FroGGstack      -> customized nginx as frontend
                -> nginx-sticky-modul


### tools & config

- default-branch: testing

- config for builds:
  - config.yaml

- make_froggstack
  - configure && make && make packages
- test_build
  - copy src_modules
  - make_froggstack
  - copy bin to testenv
  - testenv
    - start froggstack with batteries included
    - run test-suite
      - sticky session, http only, secure
      - upstream
      - naxsi
      - ...
- push_online
  - test_build
  - if ok
  - manual:
    - checkout master
    - merge testing
    - test_build w/oput testing
    - if ok -> push online

- build_modules.py -> update all modules via git, copy to ngx_modules


### sources

- nginx -> link to actual version in src_nginx/ngx-x.y.z -> 
- nginx_modules/ repo-version of available modules
- src_modules/$modul  ->  available modules (not in global git)


### branches

- master -> stable
- testing -> testing out new stuff, (default)
- build-$TIMESTAMP - build-branches, if -s (skip) was used


