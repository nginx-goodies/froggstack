#ifndef _NGX_HTTP_FLYNX_MANAGEMENT_INCLUDED_
#define _NGX_HTTP_FLYNX_MANAGEMENT_INCLUDED_

#include "ngx_http_flynx_common.h"

#include "ngx_http_flynx_management_handlers.h"

ngx_int_t ngx_http_flynx_check_access(ngx_cidr_t *cidr, struct sockaddr *sock);

ngx_int_t ngx_http_flynx_management_handler(ngx_http_request_t *r);

ngx_int_t ngx_http_flynx_management_get(ngx_http_request_t *r);
ngx_int_t ngx_http_flynx_management_post(ngx_http_request_t *r);
ngx_int_t ngx_http_flynx_management_put(ngx_http_request_t *r);
ngx_int_t ngx_http_flynx_management_delete(ngx_http_request_t *r);

#endif //_NGX_HTTP_FLYNX_MANAGEMENT_INCLUDED_