#include "ngx_http_flynx_upstream.h"

ngx_http_upstream_main_conf_t * 
ngx_http_flynx_upstream_main_conf(ngx_http_request_t *r) {
  if(r != NULL) {
    return ngx_http_get_module_main_conf(r, ngx_http_upstream_module);
  }
  
  return ngx_http_cycle_get_module_main_conf(ngx_cycle, ngx_http_upstream_module);
}

ngx_http_upstream_srv_conf_t * 
ngx_http_flynx_upstream_config(ngx_http_request_t *r, ngx_str_t *host) {
  ngx_uint_t                            i;
  ngx_http_upstream_srv_conf_t        **uscfp, *uscf;
  ngx_http_upstream_main_conf_t        *umcf;

  umcf = ngx_http_flynx_upstream_main_conf(r);
  uscfp = umcf->upstreams.elts;

  for (i = 0; i < umcf->upstreams.nelts; i++) {
    uscf = uscfp[i];    

    if (uscf->host.len == host->len
        && ngx_memcmp(uscf->host.data, host->data, host->len) == 0)
    {
        return uscf;
    }
  }

  return NULL;
}

ngx_http_upstream_rr_peers_t * ngx_http_flynx_merge_peers(
  ngx_http_upstream_rr_peers_t *peers, 
  ngx_http_upstream_rr_peer_t *peer, 
  ngx_uint_t peer_number,
  ngx_addr_t *addrs,
  ngx_uint_t addrs_number,
  ngx_flynx_data_peer_defaults_t *defaults) {
  u_char str[1024];
  ngx_uint_t i;
  
  ngx_http_upstream_rr_peers_t *new_peers;
  
  // need to alloc a new rr_peers_t as the new data won't fit the old
  // alloc rr_peers_t + new number of rr_peer_t - 1 for the one already in the rr_peers_t
  new_peers = ngx_pcalloc(ngx_cycle->pool, sizeof(ngx_http_upstream_rr_peers_t)
                + sizeof(ngx_http_upstream_rr_peer_t) * ((peer_number + addrs_number) - 1));
                
  // copy over old rr_peers_t data
  ngx_memcpy(new_peers, peers, sizeof(ngx_http_upstream_rr_peers_t));

  // alloc temp space for array construction
  ngx_http_upstream_rr_peer_t *temp = ngx_pcalloc(ngx_cycle->pool, 
  peer_number*sizeof(ngx_http_upstream_rr_peer_t) +
  addrs_number*sizeof(ngx_http_upstream_rr_peer_t));
  
  // copy over old peers
  if(peer && peer_number > 0) {
    ngx_memcpy(temp, peer, peer_number*sizeof(ngx_http_upstream_rr_peer_t));
  }
  
  // new_peer is the start of new rr_peer_t data
  ngx_http_upstream_rr_peer_t *new_peer = temp + peer_number;
  ngx_http_upstream_rr_peer_t *running = new_peer;
  
  for(i = 0; i < addrs_number; i++) {    
    // allocate and copy over sockaddr from received message
    running->sockaddr = ngx_pcalloc(ngx_cycle->pool, addrs[i].socklen);
    ngx_memcpy(running->sockaddr, addrs[i].sockaddr, addrs[i].socklen);
    running->socklen = addrs[i].socklen;
          
    ngx_memzero(&str, 1024);
  
    ngx_int_t len = ngx_flynx_sock_ntop(running->sockaddr, running->socklen, str, 1024, 1);
    running->name.data = ngx_pcalloc(ngx_cycle->pool, len);
    ngx_memcpy(running->name.data, &str, len);
    running->name.len = len;
    
    running->max_fails = defaults->max_fails;
    running->fail_timeout = defaults->fail_timeout;
    running->down = defaults->down;
    running->weight = defaults->weight;
    running->effective_weight = defaults->weight;
    running->current_weight = 0;
    
    running++;
  }

  // copy the whole thing over to the new rr_peers_t
  ngx_memcpy(new_peers->peer, temp, (peer_number + addrs_number)*sizeof(ngx_http_upstream_rr_peer_t));
  // update the number
  new_peers->number = peer_number + addrs_number;
  
  return new_peers;
}

ngx_int_t
ngx_http_flynx_data_updown_handler(void *data) {  
  ngx_flynx_data_updown_t *p = data;
  ngx_uint_t n;
  ngx_int_t i;
  
  ngx_http_upstream_srv_conf_t *uscf = ngx_http_flynx_upstream_config(NULL, &p->host);
  
  if(!uscf) {
    return 0;
  }
  
  ngx_addr_t *addrs = p->addrs;
  ngx_http_upstream_rr_peers_t *peers = uscf->peer.data;
  ngx_addr_t addr;
  ngx_http_upstream_rr_peer_t *peer;
  
  ngx_uint_t number = peers->number;
  
  for(n = 0; n < number; n++) {
    peer = &peers->peer[n];
    
    for(i = 0; i < p->naddrs; i++) {
      addr = addrs[i];
    
      if(ngx_flynx_sockaddr_equal(peer->sockaddr, addr.sockaddr)) {
        peer->down = p->down;
      }
    }
  }
  
  return 1;
}

ngx_int_t
ngx_http_flynx_data_add_peer_handler(void *data) {  
  ngx_flynx_data_peer_list_t *p = data;
  ngx_http_upstream_srv_conf_t *uscf = ngx_http_flynx_upstream_config(NULL, &p->host);
  
  if(!uscf) {
    return 0;
  }
  
  ngx_http_upstream_rr_peers_t *peers = uscf->peer.data;
  
  ngx_http_upstream_rr_peers_t *generated = ngx_http_flynx_merge_peers(
    peers, 
    peers->peer, 
    peers->number, 
    p->addrs, 
    p->naddrs,
    p->defaults);
  
    
  // set rr_peers_t to old config
  uscf->peer.data = generated;
  
  // free old rr_peers_t
  ngx_pfree(ngx_cycle->pool, peers);
  
  return 1;
}

ngx_int_t ngx_http_flynx_data_delete_peer_handler(void *data) {
  ngx_flynx_data_updown_t *p = data;
  ngx_uint_t n;
  ngx_int_t i;
  
  ngx_http_upstream_srv_conf_t *uscf = ngx_http_flynx_upstream_config(NULL, &p->host);
  
  if(!uscf) {
    return 0;
  }
  
  ngx_addr_t *addrs = p->addrs;
  ngx_http_upstream_rr_peers_t *peers = uscf->peer.data;
  ngx_addr_t addr;
  ngx_http_upstream_rr_peer_t *peer;
  
  ngx_uint_t number = peers->number;
  
  ngx_http_upstream_rr_peer_t *new_peer = ngx_pcalloc(ngx_cycle->pool, sizeof(ngx_http_upstream_rr_peer_t)*number);
  ngx_http_upstream_rr_peer_t *running = new_peer;
  ngx_uint_t new_count = 0;
  
  for(n = 0; n < number; n++) {
    peer = &peers->peer[n];
    ngx_int_t deleted = 0;
    
    for(i = 0; i < p->naddrs; i++) {
      addr = addrs[i];
    
      if(ngx_flynx_sockaddr_equal(peer->sockaddr, addr.sockaddr)) {
        deleted = 1;
        break;
      }
    }
    
    if(!deleted) {
      ngx_memcpy(running, peer, sizeof(ngx_http_upstream_rr_peer_t));
      running++;
      new_count++;
    }
  }
  
  ngx_http_upstream_rr_peers_t *new_peers = ngx_pcalloc(ngx_cycle->pool, 
    sizeof(ngx_http_upstream_rr_peers_t) +
    sizeof(ngx_http_upstream_rr_peer_t) * (new_count - 1));
    
  ngx_memcpy(new_peers, peers, sizeof(ngx_http_upstream_rr_peers_t));
  ngx_memcpy(new_peers->peer, new_peer, sizeof(ngx_http_upstream_rr_peer_t)*new_count);
  new_peers->number = new_count;
  
  uscf->peer.data = new_peers;
  
  ngx_pfree(ngx_cycle->pool, peers);
  ngx_pfree(ngx_cycle->pool, new_peer);  
  
  return 1;
}

ngx_int_t
ngx_http_flynx_data_replace_peer_handler(void *data) { 
  ngx_flynx_data_peer_list_t *p = data;
  ngx_http_upstream_srv_conf_t *uscf = ngx_http_flynx_upstream_config(NULL, &p->host);
  
  if(!uscf) {
    return 0;
  }
  
  ngx_http_upstream_rr_peers_t *peers = uscf->peer.data;
  
  ngx_flynx_data_peer_defaults_t defaults;
  defaults.weight = 1;
  defaults.max_fails = 1;
  defaults.fail_timeout = 10;
  defaults.down = 0;
  
  ngx_http_upstream_rr_peers_t *generated = ngx_http_flynx_merge_peers(
    peers, 
    NULL, 
    0, 
    p->addrs, 
    p->naddrs,
    &defaults);
  
    
  // set rr_peers_t to old config
  uscf->peer.data = generated;
  
  // free old rr_peers_t
  ngx_pfree(ngx_cycle->pool, peers);
  
  return 1;
}

void ngx_http_flynx_data_peer_list_alloc(ngx_slab_pool_t *shpool, void *original, void *data) {
  ngx_flynx_data_peer_list_t *dap_orig = original;
  ngx_flynx_data_peer_list_t *dap = data;
  
  ngx_int_t i;
  
  dap->addrs = ngx_slab_alloc_locked(shpool, sizeof(ngx_addr_t)*dap_orig->naddrs);
  ngx_memcpy(dap->addrs, dap_orig->addrs, sizeof(ngx_addr_t)*dap_orig->naddrs);
  dap->naddrs = dap_orig->naddrs;
  
  for(i = 0; i < dap_orig->naddrs; i++) {
    ngx_addr_t *addr = &dap->addrs[i];
    ngx_addr_t *addr_orig = &dap_orig->addrs[i];
    
    addr->sockaddr = ngx_slab_alloc_locked(shpool, addr_orig->socklen);
    ngx_memcpy(addr->sockaddr, addr_orig->sockaddr, addr_orig->socklen);
    addr->socklen = addr_orig->socklen;
  }
  
  dap->host.data = ngx_slab_alloc_locked(shpool, dap_orig->host.len);
  ngx_memcpy(dap->host.data, dap_orig->host.data, dap_orig->host.len);
  dap->host.len = dap_orig->host.len;
  
  if(dap_orig->defaults) {
    dap->defaults = ngx_slab_alloc_locked(shpool, sizeof(ngx_flynx_data_peer_defaults_t));
    ngx_memcpy(dap->defaults, dap_orig->defaults, sizeof(ngx_flynx_data_peer_defaults_t));
  } 
}
void ngx_http_flynx_data_peer_list_free(ngx_slab_pool_t *shpool, void *data) {
  ngx_flynx_data_peer_list_t *dap = data;
  ngx_int_t i;
  
  if(dap->addrs) {
    for(i = 0; i < dap->naddrs; i++) {
      ngx_addr_t *addr = &dap->addrs[i];
      if(addr->sockaddr) {
        ngx_slab_free_locked(shpool, addr->sockaddr);
      }
    }
    
    ngx_slab_free_locked(shpool, dap->addrs);
  }
  
  if(dap->host.data) {
    ngx_slab_free_locked(shpool, dap->host.data);
  }
  
  if(dap->defaults) {
    ngx_slab_free_locked(shpool, dap->defaults);
  }
}

ngx_int_t
ngx_flynx_sockaddr_equal(struct sockaddr *a, struct sockaddr *b) {
    struct sockaddr_in   *sin1, *sin2;
#if (NGX_HAVE_INET6)
    struct sockaddr_in6  *sin61, *sin62;
#endif

  if(a->sa_family != b->sa_family) {
    return 0;
  }

#if (NGX_HAVE_INET6)
    if(a->sa_family == AF_INET6) {
      sin61 = (struct sockaddr_in6 *) a;
      sin62 = (struct sockaddr_in6 *) b;

      if (sin61->sin6_port != sin62->sin6_port) {
          return 0;
      }

      if (ngx_memcmp(&sin61->sin6_addr, &sin62->sin6_addr, 16) != 0) {
          return 0;
      }
    }
#endif
  
  if(a->sa_family == AF_INET) {
    sin1 = (struct sockaddr_in *) a;
    sin2 = (struct sockaddr_in *) b;

    if (sin1->sin_port != sin2->sin_port) {
        return 0;
    }

    if (sin1->sin_addr.s_addr != sin2->sin_addr.s_addr) {
        return 0;
    }
  }
  
  return 1;
}