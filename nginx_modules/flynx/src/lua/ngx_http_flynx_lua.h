#ifndef _NGX_HTTP_FLYNX_LUA_INCLUDED_
#define _NGX_HTTP_FLYNX_LUA_INCLUDED_

#include <ngx_http_lua_api.h>

#include "../ngx_http_flynx_common.h"

int ngx_http_flynx_register_lua(lua_State *L);

#endif
