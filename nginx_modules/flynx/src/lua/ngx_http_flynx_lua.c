#include <lauxlib.h>

#include "ngx_http_flynx_lua.h"

#include "../ngx_http_flynx_synchronisation.h"
#include "../ngx_http_flynx_upstream.h"
#include "../ngx_http_flynx_body.h"

static int ngx_http_flynx_lua_status(lua_State *L);
static int ngx_http_flynx_lua_upstreams(lua_State *L);
static int ngx_http_flynx_lua_add_peers(lua_State *L);
static int ngx_http_flynx_lua_replace_peers(lua_State *L);
static int ngx_http_flynx_lua_delete_peers(lua_State *L);
static int ngx_http_flynx_lua_peers(lua_State *L);
static int ngx_http_flynx_lua_set_peer_down(lua_State *L);

static ngx_flynx_data_peer_list_t *ngx_http_flynx_lua_peer_list(lua_State *L, ngx_pool_t *pool, ngx_int_t *version);
static int ngx_http_flynx_lua_send_msg(lua_State *L, ngx_flynx_data_t *data);

int ngx_http_flynx_register_lua(lua_State *L) {
  lua_createtable(L, 0, 1);
  
  lua_createtable(L, 0, 1);
  lua_createtable(L, 1, 0);
  lua_pushcfunction(L, ngx_http_flynx_lua_status);
  lua_setfield(L, -2, "__index");
  lua_setmetatable(L, -2);
  lua_setfield(L, -2, "status");
  
  lua_pushcfunction(L, ngx_http_flynx_lua_upstreams);
  lua_setfield(L, -2, "upstreams");
  
  lua_pushcfunction(L, ngx_http_flynx_lua_add_peers);
  lua_setfield(L, -2, "add_peers");
  
  lua_pushcfunction(L, ngx_http_flynx_lua_replace_peers);
  lua_setfield(L, -2, "replace_peers");
  
  lua_pushcfunction(L, ngx_http_flynx_lua_delete_peers);
  lua_setfield(L, -2, "delete_peers");
  
  lua_pushcfunction(L, ngx_http_flynx_lua_peers);
  lua_setfield(L, -2, "peers");
  
  lua_pushcfunction(L, ngx_http_flynx_lua_set_peer_down);
  lua_setfield(L, -2, "set_peer_down");
  
  return 1;
}

static int
ngx_http_flynx_lua_status(lua_State *L) {
  ngx_int_t version = ngx_flynx_global_ctx.sh->version;
  
  if (lua_gettop(L) != 2) {
      lua_pushstring(L, "exactly two arguments expected");
      return lua_error(L);
  }
  
  const char *arg = luaL_checkstring(L, 2);
  
  if(!ngx_strncmp(arg, "version", 7)) {
    lua_pushinteger(L, version);
  } else if (!ngx_strncmp(arg, "status", 6)) {
    lua_pushstring(L, version > 1 ? "ok" : "default");
  } else {
    return luaL_error(L, "could not find field");
  }
  
  return 1;
}

static int
ngx_http_flynx_lua_upstreams(lua_State *L) {
  ngx_uint_t                          i;
  ngx_http_upstream_srv_conf_t        **uscfp, *uscf;
  ngx_http_upstream_main_conf_t        *umcf;
  
  if (lua_gettop(L) != 0) {
      lua_pushstring(L, "no argument expected");
      return lua_error(L);
  }

  umcf = ngx_http_flynx_upstream_main_conf(NULL);
  uscfp = umcf->upstreams.elts;
  
  lua_createtable(L, umcf->upstreams.nelts, 0);
  
  for(i = 0; i < umcf->upstreams.nelts; i++) {
    uscf = uscfp[i];
    lua_pushlstring(L, (char *)uscf->host.data, uscf->host.len);
    lua_rawseti(L, -2, i+1);
  }
  
  return 1;
}

static int 
ngx_http_flynx_lua_add_peers(lua_State *L) {
  ngx_http_request_t *r;
  ngx_int_t version;
  
  r = ngx_http_lua_get_request(L);
  
  ngx_flynx_data_peer_list_t *ap = ngx_http_flynx_lua_peer_list(L, r->pool, &version);
  ngx_flynx_data_t d = ngx_flynx_data_add_peer(ap, version);
  
  return ngx_http_flynx_lua_send_msg(L, &d);
}

static int 
ngx_http_flynx_lua_replace_peers(lua_State *L) {
  ngx_http_request_t *r;
  ngx_int_t version;
  
  r = ngx_http_lua_get_request(L);
  
  ngx_flynx_data_peer_list_t *ap = ngx_http_flynx_lua_peer_list(L, r->pool, &version);
  ngx_flynx_data_t d = ngx_flynx_data_replace_peer(ap, version);
  
  return ngx_http_flynx_lua_send_msg(L, &d);
}

static int 
ngx_http_flynx_lua_delete_peers(lua_State *L) {
  ngx_http_request_t *r;
  ngx_int_t version;
  
  r = ngx_http_lua_get_request(L);
  
  lua_pushnil(L);
  lua_insert(L, 3);
  
  ngx_flynx_data_peer_list_t *ap = ngx_http_flynx_lua_peer_list(L, r->pool, &version);
  ngx_flynx_data_t d = ngx_flynx_data_delete_peer(ap, version);
  
  return ngx_http_flynx_lua_send_msg(L, &d);
}

static int
ngx_http_flynx_lua_peers(lua_State *L) {
  ngx_str_t host;
  ngx_uint_t                          i;
  ngx_http_upstream_srv_conf_t        *uscf;
  ngx_http_upstream_rr_peers_t *peers;
  ngx_http_upstream_rr_peer_t *peer;
  u_char str[1024];
  
  if (lua_gettop(L) != 1) {
      lua_pushstring(L, "expecting exactly one argument");
      return lua_error(L);
  }
  
  host.data = (u_char *) luaL_checklstring(L, 1, &host.len);
  
  uscf = ngx_http_flynx_upstream_config(NULL, &host);
  
  if(!uscf) {
    return luaL_error(L, "not a valid upstream");
  }
  peers = uscf->peer.data;
  
  lua_createtable(L, peers->number, 0);
  
  
  for(i = 0; i < peers->number; i++) {
    peer = &peers->peer[i];
    ngx_memzero(&str, 1024);
  
    ngx_flynx_sock_ntop(peer->sockaddr, peer->socklen, str, 1024, 1);
    
    lua_createtable(L, 5, 0);
    
    lua_pushstring(L, (char*)str);
    lua_setfield(L, -2, "uri");
    
    lua_pushinteger(L, peer->down);
    lua_setfield(L, -2, "down");
    
    lua_pushinteger(L, peer->weight);
    lua_setfield(L, -2, "weight");
    
    lua_pushinteger(L, peer->max_fails);
    lua_setfield(L, -2, "max_fails");
    
    lua_pushinteger(L, peer->fail_timeout);
    lua_setfield(L, -2, "fail_timeout");
    
    lua_rawseti(L, -2, i+1);
  }
  
  return 1;
}

static int
ngx_http_flynx_lua_set_peer_down(lua_State *L) {
  ngx_str_t host;
  ngx_str_t peer;
  ngx_str_t down;
  ngx_int_t version;
  ngx_http_request_t *r;
  ngx_array_t *domains;
  ngx_array_t *addrs;
  ngx_str_t *cur;
  ngx_flynx_data_updown_t *updown;
  
  if (lua_gettop(L) != 4) {
      lua_pushstring(L, "expecting exactly four arguments");
      return lua_error(L);
  }
  
  host.data = (u_char *) luaL_checklstring(L, 1, &host.len);
  peer.data = (u_char *) luaL_checklstring(L, 2, &peer.len);
  down.data = (u_char *) luaL_checklstring(L, 3, &down.len);
  version = luaL_checkint(L, 4);
  
  r = ngx_http_lua_get_request(L);
  
  domains = ngx_array_create(r->pool, 1, sizeof(ngx_str_t));
  cur = ngx_array_push(domains);
  *cur = peer;
  
  addrs = ngx_flynx_parse_uris(r->pool, domains);
  
  updown = ngx_pcalloc(r->pool, sizeof(ngx_flynx_data_updown_t));
  updown->down = *down.data == 'd';
  updown->host = host;
  updown->addrs = addrs->elts;
  updown->naddrs = addrs->nelts; 
  
  ngx_flynx_data_t data = ngx_flynx_data_updown(updown, version);
  
  return ngx_http_flynx_lua_send_msg(L, &data);
}

static ngx_flynx_data_peer_list_t *ngx_http_flynx_lua_peer_list(
  lua_State *L, 
  ngx_pool_t *pool,
  ngx_int_t *version) {
  
  int i;
  ngx_array_t *domains;
  ngx_str_t *cur;
  ngx_array_t *addrs;
  ngx_str_t host;
  ngx_int_t read_defaults;
  ngx_flynx_data_peer_defaults_t *defaults = ngx_pcalloc(pool, sizeof(ngx_flynx_data_peer_defaults_t));
  
  read_defaults = !lua_isnil(L, 3);
  
  host.data = (u_char *) luaL_checklstring(L, 1, &host.len);
  
  luaL_checktype (L, 2, LUA_TTABLE);
  
  if(read_defaults) {
    luaL_checktype (L, 3, LUA_TTABLE);
  }
  
  *version = luaL_checkint(L, 4);
  
  // read domains
  
  ngx_int_t len = lua_objlen(L, 2);
  domains = ngx_array_create(pool, len, sizeof(ngx_str_t));
  
  for(i = 0; i < len; i++) {
    lua_rawgeti(L, 2, i+1);
    cur = ngx_array_push(domains);
    cur->data = (u_char *) luaL_checklstring(L, -1, &cur->len);
  }
  
  addrs = ngx_flynx_parse_uris(pool, domains);
  
  
  // read defaults
  defaults->down = 0;
  defaults->max_fails = 1;
  defaults->weight = 1;
  defaults->fail_timeout = 10;
  
  if(read_defaults) {
    lua_pushstring(L, "down");
    lua_gettable(L, 3);
  
    if(!lua_isnil(L, -1)) {
      defaults->down = lua_toboolean(L, -1);
    }
  
    lua_pushstring(L, "max_fails");
    lua_gettable(L, 3);
  
    if(!lua_isnil(L, -1)) {
      defaults->max_fails = lua_tointeger(L, -1);
    }
  
    lua_pushstring(L, "fail_timeout");
    lua_gettable(L, 3);
  
    if(!lua_isnil(L, -1)) {
      defaults->fail_timeout = lua_tointeger(L, -1);
    }
  
    lua_pushstring(L, "weight");
    lua_gettable(L, 3);
  
    if(!lua_isnil(L, -1)) {
      defaults->weight = lua_tointeger(L, -1);
    }
  
    lua_pop(L, 4);
  }
  
  
  ngx_flynx_data_peer_list_t *ap = ngx_pcalloc(pool, sizeof(ngx_flynx_data_peer_list_t));
  
  ap->host = host;
  ap->addrs = addrs->elts;
  ap->naddrs = addrs->nelts; 
  ap->defaults = defaults;
  
  return ap;
}

static int 
ngx_http_flynx_lua_send_msg(lua_State *L, ngx_flynx_data_t *data) {  
  ngx_int_t result;

  if((result = ngx_http_flynx_send_msg(data))) {    
    if(result == NGX_FLYNX_MANAGEMENT_VERSION_MISMATCH) {
      return luaL_error(L, "[flynx] version mismatch. Version needs to be greater than the current version");
    }
    
    return luaL_error(L, "could not propagate change to workers");
  }
  
  lua_pushboolean(L, 1);
  return 1;
}