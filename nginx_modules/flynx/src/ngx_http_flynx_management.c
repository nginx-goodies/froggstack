#include "ngx_http_flynx_management.h"

#include "ngx_http_flynx_body.h"

ngx_int_t ngx_http_flynx_check_access(ngx_cidr_t *cidr, struct sockaddr *sock) {
    struct sockaddr_in          *sin;
    in_addr_t                    addr;
#if (NGX_HAVE_INET6)
    ngx_int_t n;
    u_char                      *p;
    struct sockaddr_in6         *sin6;
#endif

  // only have one allowed CIDR, so if family differs, decline
  if(sock->sa_family != cidr->family) {
    return NGX_DECLINED;
  }
  
  switch(sock->sa_family) {
#if (NGX_HAVE_INET6)
    case AF_INET6:
      sin6 = (struct sockaddr_in6 *)sock;
      p = sin6->sin6_addr.s6_addr;

      for (n = 0; n < 16; n++) {
          if ((p[n] & cidr->u.in6.mask.s6_addr[n]) != cidr->u.in6.addr.s6_addr[n]) {
              // part of ipv6 address didn't match, decline
              return NGX_DECLINED;
          }
      }

      break;
#endif

    default:
      sin = (struct sockaddr_in *)sock;
      addr = sin->sin_addr.s_addr;
      if((addr & cidr->u.in.mask) != cidr->u.in.addr) {
        // ipv4 address didn't match, decline
        return NGX_DECLINED;
      }
      break;
  }
  
  // everything matched, allow
  return NGX_OK;
}


ngx_int_t
ngx_http_flynx_management_handler(ngx_http_request_t *r) {
  ngx_http_flynx_loc_conf_t *conf;
  ngx_http_core_loc_conf_t *loc;
  ngx_http_flynx_ctx_t *ctx;
  conf = ngx_http_get_module_loc_conf(r, ngx_http_flynx_module);
  loc = ngx_http_get_module_loc_conf(r, ngx_http_core_module);
   
  if(!conf->management) {
    return NGX_HTTP_FORBIDDEN;    
  }
  
  if(ngx_http_flynx_check_access(&conf->management_allow, r->connection->sockaddr) != NGX_OK) {
    return NGX_HTTP_FORBIDDEN;
  }
  
  if(conf->management == NGX_FLYNX_MANAGEMENT_READONLY && r->method != NGX_HTTP_GET) {
    return NGX_HTTP_FORBIDDEN;
  }

  if(r->method == NGX_HTTP_HEAD) {
    return NGX_HTTP_FORBIDDEN;
  }
  
  ngx_array_t *path = ngx_flynx_parse_path(r->pool, &r->uri, &loc->name);
  
  if(path->nelts == 0) {
    return NGX_HTTP_NOT_FOUND;
  }
  
  ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_flynx_ctx_t));
  ctx->path = path;
  ngx_http_set_ctx(r, ctx, ngx_http_flynx_module);
  
  switch(r->method) {
    case NGX_HTTP_GET:
      return ngx_http_flynx_management_get(r);
      break;
    case NGX_HTTP_POST:
      return ngx_http_flynx_management_post(r);
      break;
    case NGX_HTTP_PUT:
      return ngx_http_flynx_management_put(r);
      break;
    case NGX_HTTP_DELETE:
      return ngx_http_flynx_management_delete(r);
      break;    
    default:
      return NGX_HTTP_FORBIDDEN;
  }
  
  return NGX_HTTP_NOT_FOUND;
}

ngx_int_t
ngx_http_flynx_management_get(ngx_http_request_t *r) {
  ngx_http_flynx_ctx_t *ctx;
  
  ctx = ngx_http_get_module_ctx(r, ngx_http_flynx_module);
  ngx_str_t *resource = ctx->path->elts;
  
  switch(ctx->path->nelts) {
    case 1:
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"ping", 4)) {
        return ngx_http_flynx_ping(r);
      }
      
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"upstream", 8)) {
        return ngx_http_flynx_upstream_list(r);
      }
      
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"snapshot", 8)) {
        return ngx_http_flynx_snapshot(r);
      }
      
      break;
    case 2:
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"upstream", 8)) {
        return ngx_http_flynx_upstream_detail(r, &resource[1]);
      }
      
      break;
  }
  
  return NGX_HTTP_NOT_FOUND;
}

ngx_int_t
ngx_http_flynx_management_post(ngx_http_request_t *r) {
  ngx_http_flynx_ctx_t *ctx;
  
  ctx = ngx_http_get_module_ctx(r, ngx_http_flynx_module);
  ngx_str_t *resource = ctx->path->elts;
  ngx_http_flynx_upstream_ctx_t *up = ngx_pcalloc(r->pool, sizeof(ngx_http_flynx_upstream_ctx_t));
  ctx->data = up;
  
  switch(ctx->path->nelts) {
    case 2:
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"upstream", 8)) {
        up->alloc = NULL;
        up->data = ngx_http_flynx_ctx_add_data;
        return ngx_http_flynx_body_handler(r, ngx_http_flynx_upstream_peer_list_handler);
      }
    case 3:
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"upstream", 8)) {
        
      
        if(!ngx_strncasecmp(resource[2].data, (u_char*)"up", 2)) {
          up->alloc = ngx_http_flynx_ctx_updown_alloc;
          up->data = ngx_http_flynx_ctx_updown_data;

        } else if(!ngx_strncasecmp(resource[2].data, (u_char*)"down", 4)) {
          up->alloc = ngx_http_flynx_ctx_updown_alloc;
          up->data = ngx_http_flynx_ctx_updown_data;

        } else if(!ngx_strncasecmp(resource[2].data, (u_char*)"delete", 6)) {
          up->alloc = NULL;
          up->data = ngx_http_flynx_ctx_delete_data;
        } else {
        
          break;
        }
        
        return ngx_http_flynx_body_handler(r, ngx_http_flynx_upstream_peer_list_handler);
      }

      break;
  }
  
  return NGX_HTTP_NOT_FOUND;
}

ngx_int_t
ngx_http_flynx_management_put(ngx_http_request_t *r) {
  ngx_http_flynx_ctx_t *ctx;
  
  ctx = ngx_http_get_module_ctx(r, ngx_http_flynx_module);
  ngx_str_t *resource = ctx->path->elts;
  ngx_http_flynx_upstream_ctx_t *up = ngx_pcalloc(r->pool, sizeof(ngx_http_flynx_upstream_ctx_t));
  ctx->data = up;
  
  switch(ctx->path->nelts) {
    case 2:
      if(!ngx_strncasecmp(resource[0].data, (u_char*)"upstream", 8)) {
        up->alloc = NULL;
        up->data = ngx_http_flynx_ctx_replace_data;
        return ngx_http_flynx_body_handler(r, ngx_http_flynx_upstream_peer_list_handler); 
      }
      break;
  }
  
  return NGX_HTTP_NOT_FOUND;
}

ngx_int_t
ngx_http_flynx_management_delete(ngx_http_request_t *r) {
  ngx_http_flynx_ctx_t *ctx;
  
  ctx = ngx_http_get_module_ctx(r, ngx_http_flynx_module);
  //ngx_str_t *resource = ctx->path->elts;
  
  switch(ctx->path->nelts) {
  }
  
  return NGX_HTTP_NOT_FOUND;
}