#ifndef _NGX_HTTP_FLYNX_COMMON_INCLUDED_
#define _NGX_HTTP_FLYNX_COMMON_INCLUDED_
#include <nginx.h>
#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include <ngx_array.h>

#define ngx_flynx_add_timer(ev, timeout)                                      \
    if (!ngx_exiting && !ngx_quit) ngx_add_timer(ev, (timeout))
    
#define NGX_FLYNX_SHM_NAME_LEN 256

#define NGX_FLYNX_DATA_INIT 0
#define NGX_FLYNX_DATA_ADD_PEER 1
#define NGX_FLYNX_DATA_UPDOWN 2
#define NGX_FLYNX_DATA_REPLACE_PEER 4
#define NGX_FLYNX_DATA_DELETE_PEER 8

#define NGX_FLYNX_MANAGEMENT_OK 0
#define NGX_FLYNX_MANAGEMENT_ERROR 1
#define NGX_FLYNX_MANAGEMENT_VERSION_MISMATCH 2

#define NGX_FLYNX_MANAGEMENT_OFF 0
#define NGX_FLYNX_MANAGEMENT_ON 1
#define NGX_FLYNX_MANAGEMENT_READONLY 2
    
typedef void (*ngx_http_flynx_data_alloc_pt)(ngx_slab_pool_t *shpool, void *original, void *data);
typedef void (*ngx_http_flynx_data_free_pt)(ngx_slab_pool_t *shpool, void *data);
typedef ngx_int_t (*ngx_http_flynx_data_handler_pt)(void *data);

typedef struct ngx_flynx_data_peer_defaults_s {
  ngx_int_t weight;
  ngx_uint_t max_fails;
  time_t fail_timeout;
  ngx_uint_t down;
} ngx_flynx_data_peer_defaults_t;

typedef struct ngx_flynx_data_add_peer_s {
  ngx_addr_t *addrs;
  ngx_int_t naddrs;
  ngx_str_t host;
  ngx_flynx_data_peer_defaults_t *defaults;
} ngx_flynx_data_peer_list_t;


typedef struct ngx_flynx_data_updown_s {
  ngx_addr_t *addrs;
  ngx_int_t naddrs;
  ngx_str_t host;
  ngx_flynx_data_peer_defaults_t *defaults;
  ngx_int_t down;
} ngx_flynx_data_updown_t;

#define ngx_flynx_data_add_peer(data, version) { \
    NGX_FLYNX_DATA_ADD_PEER, \
    version, \
    (data), \
    sizeof(ngx_flynx_data_peer_list_t), \
    ngx_http_flynx_data_add_peer_handler, \
    ngx_http_flynx_data_peer_list_alloc, \
    ngx_http_flynx_data_peer_list_free \
  }
  
#define ngx_flynx_data_delete_peer(data, version) { \
    NGX_FLYNX_DATA_DELETE_PEER, \
    version, \
    (data), \
    sizeof(ngx_flynx_data_peer_list_t), \
    ngx_http_flynx_data_delete_peer_handler, \
    ngx_http_flynx_data_peer_list_alloc, \
    ngx_http_flynx_data_peer_list_free \
  }
  
#define ngx_flynx_data_replace_peer(data, version) { \
    NGX_FLYNX_DATA_REPLACE_PEER, \
    version, \
    (data), \
    sizeof(ngx_flynx_data_peer_list_t), \
    ngx_http_flynx_data_replace_peer_handler, \
    ngx_http_flynx_data_peer_list_alloc, \
    ngx_http_flynx_data_peer_list_free \
  }
  
#define ngx_flynx_data_updown(data, version) { \
    NGX_FLYNX_DATA_UPDOWN, \
    version, \
    (data), \
    sizeof(ngx_flynx_data_updown_t), \
    ngx_http_flynx_data_updown_handler, \
    ngx_http_flynx_data_peer_list_alloc, \
    ngx_http_flynx_data_peer_list_free \
  }
   
#define ngx_flynx_data_init(data, version) { \
    NGX_FLYNX_DATA_INIT, \
    version, \
    (data), \
    sizeof(ngx_flynx_data_init_t), \
    ngx_http_flynx_data_init_handler, \
    NULL, \
    NULL \
  }

typedef struct ngx_flynx_data_init_s {
} ngx_flynx_data_init_t;

typedef struct ngx_flynx_data_s {
  ngx_int_t                            type;
  ngx_int_t                            version;
  void                                *data;
  ngx_int_t                            len;
  ngx_http_flynx_data_handler_pt       handler;
  ngx_http_flynx_data_alloc_pt         alloc;
  ngx_http_flynx_data_free_pt          free;
} ngx_flynx_data_t;

typedef struct ngx_flynx_msg_s {
    ngx_queue_t                          queue;
    ngx_flynx_data_t                     content;
    ngx_int_t                            count;
    ngx_pid_t                           *pid;
} ngx_flynx_msg_t;

typedef struct ngx_flynx_shctx_s {
    ngx_queue_t                          msg_queue;
    ngx_int_t                            version;
} ngx_flynx_shctx_t;

typedef struct ngx_flynx_global_ctx_s {
    ngx_event_t                          msg_timer;
    ngx_slab_pool_t                     *shpool;
    ngx_flynx_shctx_t                   *sh;
} ngx_flynx_global_ctx_t;

typedef struct ngx_http_flynx_ctx_s {
    ngx_array_t *path; // array of ngx_str_t
    void *data; // specific data for post/put requests (due to callbacks)
} ngx_http_flynx_ctx_t;

typedef void* (*ngx_http_flynx_upstream_ctx_alloc_pt) (ngx_pool_t *pool);
typedef ngx_flynx_data_t* (*ngx_http_flynx_upstream_ctx_data_pt) (ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p);

typedef struct ngx_http_flynx_upstream_ctx_s {
  ngx_http_flynx_upstream_ctx_alloc_pt alloc;
  ngx_http_flynx_upstream_ctx_data_pt data;
} ngx_http_flynx_upstream_ctx_t;

typedef struct {
  ngx_str_t                      shm_name;
  ngx_uint_t                     shm_size;
} ngx_http_flynx_main_conf_t;

typedef struct {
} ngx_http_flynx_srv_conf_t;

typedef struct {
  ngx_int_t management;
  ngx_cidr_t management_allow;
} ngx_http_flynx_loc_conf_t;

#if nginx_version >= 1006000
#define ngx_flynx_sock_ntop(sa, socklen, text, len, port) ngx_sock_ntop(sa, socklen, text, len, port)
#else
#define ngx_flynx_sock_ntop(sa, socklen, text, len, port) ngx_sock_ntop(sa, text, len, port)
#endif

extern ngx_module_t ngx_http_flynx_module;

extern ngx_flynx_global_ctx_t ngx_flynx_global_ctx;
extern ngx_int_t ngx_http_flynx_generation;

#endif //_NGX_HTTP_FLYNX_COMMON_INCLUDED_