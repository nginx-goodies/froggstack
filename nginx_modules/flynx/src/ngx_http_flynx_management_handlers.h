#ifndef _NGX_HTTP_FLYNX_MANAGEMENT_HANDLERS_INCLUDED_
#define _NGX_HTTP_FLYNX_MANAGEMENT_HANDLERS_INCLUDED_

#include "ngx_http_flynx_common.h"

ngx_int_t ngx_http_flynx_ping(ngx_http_request_t *r);
ngx_int_t ngx_http_flynx_snapshot(ngx_http_request_t *r);
ngx_int_t ngx_http_flynx_upstream_list(ngx_http_request_t *r);
ngx_int_t ngx_http_flynx_upstream_detail(ngx_http_request_t *r, ngx_str_t *upstream_name);

void ngx_http_flynx_upstream_peer_list_handler(ngx_http_request_t *r);

void* ngx_http_flynx_ctx_updown_alloc(ngx_pool_t *pool);
ngx_flynx_data_t* ngx_http_flynx_ctx_add_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p);
ngx_flynx_data_t* ngx_http_flynx_ctx_delete_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p);
ngx_flynx_data_t* ngx_http_flynx_ctx_replace_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p);
ngx_flynx_data_t* ngx_http_flynx_ctx_updown_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p);

#endif // _NGX_HTTP_FLYNX_MANAGEMENT_HANDLERS_INCLUDED_