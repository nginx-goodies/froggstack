#!/bin/bash
#
#
vdir="venv" 


echo "

APP_INSTALLER 

"
echo "[i] Initializing " 

if [ -d "$vdir" ]; then
echo "  >  old venv found, creating new" 
    rm -Rf $vdir
fi
echo "  >  installing new virtual-env in $vdir" 

echo ">  installing virtualenv in $vdir" 
virtualenv $vdir

. $vdir/bin/activate

echo ">  installing modules" 
pip install setuptools
pip install distribute
pip install flask
pip install flask-sqlalchemy
pip install --upgrade simplejson
pip install matplotlib
pip install GitPython

#pip install redis
#pip install psycopg2
#pip install markdown



echo ">  configuring" 

mkdir builds

echo "

> OK ... done

"



