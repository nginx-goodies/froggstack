from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy()

from perftest import PerfTest, PerfLog

__all__ = [ 
            "db",
            "PerfTest",
            "PerfLog",
          ]
