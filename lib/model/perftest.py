from model import db
from time import time, localtime, strftime

class PerfTest(db.Model):
  __tablename__ = 'perfdata'
  __table_args__ = {'sqlite_autoincrement': True}

  id = db.Column(db.Integer, primary_key=True)
  tid = db.Column(db.Integer, nullable=False)
  test_name = db.Column(db.String(100), nullable=False)
  conns = db.Column(db.Integer, nullable=False)
  rfield = db.Column(db.String(100), nullable=False)
  res = db.Column(db.Integer, nullable=False, server_default="0")


  def __init__(self, tid, test_name, conns,  rfield, res ):
    self.tid = tid
    self.test_name = test_name 
    self.conns = conns 
    self.rfield = rfield 
    self.res = res

    
class PerfLog(db.Model):
  __tablename__ = 'perflog'
  __table_args__ = {'sqlite_autoincrement': True}

  id = db.Column(db.Integer, primary_key=True)
  tid = db.Column(db.Integer, nullable=False, unique=True)
  perftest_name = db.Column(db.String(100), nullable=False, server_default=strftime("%F %H:%M", localtime(time())))

  def __init__(self, tid, perftest_name):
    self.tid = tid
    self.perftest_name = perftest_name 
