
# perftest and ordinary builds
#
# default -opts: http://pastebin.com/dBC7E8Jd


compile_opts = {
  
  #     --with-cc-opt="-Os -fstack-protector -D_FORTIFY_SOURCES=2 -ffast-math -fomit-frame-pointer -falign-functions=64 -falign-loops=32 -Wno-error"


  'default_opts': """
    --with-cc-opt="-Os -fstack-protector -D_FORTIFY_SOURCES=2 -ffast-math -fomit-frame-pointer -falign-functions=64 -falign-loops=32 -Wno-error"
    --conf-path=/etc/nginx/nginx.conf 
    --sbin-path=/usr/sbin/nginx
    --prefix=
    --pid-path=/var/run/nginx/nginx.pid
    --error-log-path=/var/log/nginx/error.log
    --http-log-path=/var/log/nginx/access.log
    --http-client-body-temp-path=/var/run/nginx/client_temp
    --http-proxy-temp-path=/var/run/nginx/proxy_temp
    --http-fastcgi-temp-path=/var/run/nginx/fastcgi_temp
    --with-file-aio
    --with-http_gzip_static_module
    --with-http_stub_status_module
    --with-http_geoip_module
    --with-http_v2_module
    --with-debug
    --with-ipv6
    --with-http_realip_module
    --without-mail_pop3_module
    --without-mail_smtp_module
    --without-mail_imap_module
    --without-http_scgi_module
    --without-http_uwsgi_module  
    --with-stream 
  """, 


  'nginx_openssl_opts': """
    --with-http_ssl_module    
    --with-openssl=$nmd/openssl
    --add-module=$nmd/nginx-openssl-version
    --add-module=$nmd/naxsi
    --add-module=$nmd/lua-nginx-module
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/echo-nginx-module
    --add-module=$nmd/nginx-accesskey
    --add-module=$nmd/ngx_http_log_request_speed
    --add-module=$nmd/set-misc-nginx-module
    --add-module=$nmd/nginx-sticky-module-ng
    --add-module=$nmd/memc-nginx-module
    --add-module=$nmd/nginx-upstream-fair
    --add-module=$nmd/headers-more-nginx-module
    --add-module=$nmd/encrypted-session-nginx-module
    --add-module=$nmd/srcache-nginx-module
    --add-module=$nmd/srcache-nginx-module
    --add-module=$nmd/form-input-nginx-module
    """, 

  # libressl-static
  'nginx_libressl_opts': """
    --with-http_ssl_module    
    --with-openssl=$nmd/libressl
    --with-cc-opt="-Wno-implicit-function-declaration -std=c99 -Wno-error"
    --add-module=$nmd/naxsi
    --add-module=$nmd/nginx-openssl-version
    --add-module=$nmd/lua-nginx-module
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/echo-nginx-module
    --add-module=$nmd/nginx-accesskey
    --add-module=$nmd/ngx_http_log_request_speed
    --add-module=$nmd/set-misc-nginx-module
    --add-module=$nmd/nginx-sticky-module-ng
    --add-module=$nmd/memc-nginx-module
    --add-module=$nmd/nginx-upstream-fair
    --add-module=$nmd/headers-more-nginx-module
    --add-module=$nmd/encrypted-session-nginx-module
    --add-module=$nmd/srcache-nginx-module
    --add-module=$nmd/form-input-nginx-module
    """, 

  # (libre|open)ssl-dynamic
  'nginx_full_opts': """
    --with-http_ssl_module    
    --add-module=$nmd/naxsi
    --add-module=$nmd/nginx-openssl-version
    --add-module=$nmd/lua-nginx-module
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/echo-nginx-module
    --add-module=$nmd/nginx-accesskey
    --add-module=$nmd/ngx_http_log_request_speed
    --add-module=$nmd/set-misc-nginx-module
    --add-module=$nmd/nginx-sticky-module-ng
    --add-module=$nmd/memc-nginx-module
    --add-module=$nmd/nginx-upstream-fair
    --add-module=$nmd/headers-more-nginx-module
    --add-module=$nmd/encrypted-session-nginx-module
    --add-module=$nmd/srcache-nginx-module
    --add-module=$nmd/form-input-nginx-module
    """, 




  'nginx_flynx_opts': """
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/flynx
    
    """, 


  # v1.9.x only
  'nginx_tcp-lb_opts': """
  --with-stream
  
  
  """, 

    
  'nginx_nossl_opts': """
    --add-module=$nmd/naxsi
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/echo-nginx-module
    --add-module=$nmd/nginx-accesskey
    --add-module=$nmd/ngx_http_log_request_speed
    --add-module=$nmd/set-misc-nginx-module
    --add-module=$nmd/nginx-sticky-module-ng
    --add-module=$nmd/memc-nginx-module
    --add-module=$nmd/lua-nginx-module
    --add-module=$nmd/nginx-upstream-fair
    --add-module=$nmd/headers-more-nginx-module
    --add-module=$nmd/encrypted-session-nginx-module
    --add-module=$nmd/srcache-nginx-module   
    --add-module=$nmd/form-input-nginx-module 
    """, 

  # perftest-for comparing stuff
  'nginx_plain_opts': """
    """





}

