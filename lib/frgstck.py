#!/usr/bin/env python
#
# nginx-make:
#   - dev
# 
# v 0.3.50 - 2014-11-10
#

# from flask import current_app
from git import Repo
from copts import * 
from time import time, strftime, localtime 
import os 



def frg_help():
  print """

frg - FROGGSTACK - CRUISE CONTROL 
      for nginx-builds & pertest (batteries NOT yet included)
      v %s
      
Usage:
  ./frg [command] [options]
  
Commands:
  Builds
   -m           - make binaries
                  bins will be copied to (for $name see -n )
                    -> builds/nginx-latest-$name and
                    -> builds/$timestamp-nginx-$name
  
   -s           - skip "make clean"
   
   -T [openssl|libressl]
                - touch nginx_modules/(open|libre)ssl/.openssl/include/openssl/ssl.h to 
                  prevent nginx to run config && make && make install 
                  / compiling openssl again, usefull when using libressl
                  where you need to make tools and libs before
   
   -k           - keep this branch, dont checkout build-timestamp 
                  no commit after build; works only with -m 
                  and cannot be used on master
                  usefull for development
  
   -t [name]    - use compile_opts_template for build
                  see lib/copts.py; naming is nginx_$name_opts
                  please note: this is just a temoprary hack 
                  for not having config_files ready and might
                  get deleted on future versions
                  this works only with -m 

   -l           - list available conf_opts_templates, usefull for
                  determining the name for -t 
  
  Perftest
   -p m[ake]    - run make and perftest
   -p r[un]     - run perftest 
   -p g[enres]  - generate results
  
  Misc
   -h           - display help and exit
   -v           - display version and exit
  
  Repo-Updates
   -u           - merely just a shortcut for 
                  git pull origin master 
  
Options
   -c /path/to/alternate/config.conf; ***
                - if path omitted, file must be in conf/
                
   -n [name]    - name for perftest/build; 
                  builds: in omitted, current version will be used; bins
                  perftest: if omitted, current timestamp will be used 

   -d           - turn on debug-mode ***
    
   -v           - display version info and exit

Docs:
  see docs/install,usage
  see Readme.md for a short "About"
  
  """ % os.environ["VERSION"]

def fd(put_out):
  if os.environ["DEBUG"] == "1":
    print "d: %s" % put_out
  return(0)

def copts_list():

  clist = []
  for opts in compile_opts:
    try:
      apts = opts.split("_")[1]
      if apts == "opts":
        continue 
      clist.append(apts)
    except:
      continue
  return(sorted(clist))


def ehlo():
  print """

FROGGSTACK - automated nginx-build-environment 
             (batteries not yet included)
  
  """

def make_bins():
  print "> making nginx bins"
  tstamp = int(time())
  o_dir = os.environ["OUT_DIR"] 
  cdir = os.environ["CDIR"]
  name_set = os.environ["NAME_SET"]  
  skip_clean = os.environ["SKIP_CLEAN"]  
  copts_name = os.environ["COPTS_NAME"]  
  keep_branch = os.environ["KEEP_BRANCH"]  
  touch_openssl = os.environ["TOUCH_OPENSSL"]
  exit_after_opts = os.environ["EXIT_AFTER_OPTS"]

  print " > configure_opts: %s " % copts_name
  # creating nginx-full 
  print " > making nginx "
  try:
    confopts_name = compile_opts["nginx_%s_opts" % copts_name].strip()
    confopts_default =  compile_opts["default_opts"].strip()
    confopts = confopts_default + "\n    " + confopts_name
  except:
    confopts = None

  print confopts

  if exit_after_opts == "1":
    return()

  if not confopts:
    print "[-] ERROR, cannot find nginx_%s_opts in lib/copts.py" % copts_name
    print "> available templates  (see -t [name])"
    for c in copts_list():
      print "   > %s " % c
    sys.exit(2)
  tstamp = int(time())
  tname = strftime("%F-%H_%M", localtime(float(tstamp)))

  if not os.path.isdir("%s/builds" % cdir):
    os.mkdir("%s/builds" % cdir)
    
  bbranch = "build-%s" % tname
  repo = Repo('./')
  my_branch = repo.active_branch
  if keep_branch == "1":
    if my_branch == "master":
      print """

[-] ERROR, you cannot use -k on branch master

      """
      sys.exit(2)
    
  if keep_branch == "0":
    os.system("""git branch %s""" % bbranch)
    os.system("""git checkout %s""" % bbranch)
  # hack for libressl-temporary
  if touch_openssl == "0":
    touch_sslh = """echo "no_touch" """
  else:
    touch_sslh = "sleep 2 && touch ../../nginx_modules/%s/.openssl/include/openssl/ssl.h" % (touch_openssl)
  confopts = confopts.replace("\n", "")
  os.system("""nmd=%s/nginx_modules && export nmd && cd %s/nginx && ./configure %s && %s && make """ % (cdir, cdir, confopts, touch_sslh))

  if name_set == "0":
    try:
      gname = os.popen("%s/nginx/objs/nginx -V 2>&1""" % (cdir)).readlines()[0].split("/")[1]
    except:
      gname="default"
  else:
    gname = name_set.replace(" ", "_")  

  os.system("""cp %s/nginx/objs/nginx %s/builds/%s-nginx-%s""" % (cdir, cdir, tstamp, gname))
  os.system("""cp %s/nginx/objs/nginx %s/builds/nginx-latest-%s""" % (cdir, cdir, gname))
  os.system("""cp %s/nginx/objs/nginx %s/builds/nginx-latest""" % (cdir, cdir))

  if keep_branch == "0":
    if skip_clean == "0":
      os.system("""cd %s/nginx && make clean""" % (cdir))
      os.system("""git stash""")
      
      os.system("""git checkout %s""" % my_branch)
      os.system("""git branch -d %s""" % bbranch)
  
    else:
      print " > skipping make clean"
      os.system("""git add ./""")
      os.system("""git commit -a -m "build-commit %s / %s" """% (bbranch, tstamp))
  
  
    os.system("""git checkout %s""" % my_branch)    
  print " > done making nginx-%s" % gname
 
    
