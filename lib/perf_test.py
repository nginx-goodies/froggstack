#!/usr/bin/env python
#
#
# nginx-make:
#   - dev
#   - plain
#   - stable
# 

import os, sys, yaml, csv
from time import strftime, localtime, time
from shutil import copy as cp
#import sqlite3 as db
from flask import current_app
from model import *
from copts import * 
from git import Repo

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties


def make_perftest_bins():
  print "> making perftest bins"
  o_dir = current_app.config["OUT_DIR"] 
  cdir = current_app.config["CDIR"]
  #for x in ["plain"]:
  tstamp = int(time())
  tname = strftime("%F-%H_%M", localtime(float(tstamp)))

  bbranch = "perftest-build-%s" % tname
  repo = Repo('./')
  my_branch = repo.active_branch
  os.system("""git branch %s""" % bbranch)
  os.system("""git checkout %s""" % bbranch)
  for x in ["full", "plain"]:
    # creating nginx-full 
    print " > making nginx - %s" % x
    confopts = compile_opts["nginx_%s_opts" % x]
    try:
      os.system("""nmd=%s/nginx_modules && export nmd && cd %s/nginx && ./configure  `echo "%s" |  perl -ne 'chomp and print'` && make """ % (cdir, cdir, confopts))
      os.system("""cp %s/nginx/objs/nginx %s/testenv/nginx-%s""" % (cdir, cdir, x))
      os.system("""cp %s/nginx/objs/nginx %s/testenv/bin/nginx-%s.%s""" % (cdir, cdir, x, tstamp))
      print " > done making nginx-%s" % x
    except:
      continue
  os.system("""git stash""")
  os.system("""git checkout %s""" % my_branch)
  os.system("""git branch -d %s""" % bbranch)


def run_perftest():
  print " > runnign perftest"
  tid = int(time())
  test_server = "127.0.0.1"
  test_port = "4202"
  test_env = "testenv"
  o_dir = current_app.config["OUT_DIR"]
  test_runs = current_app.config["TEST_RUNS"]
  cdir = current_app.config["CDIR"]
  pname = current_app.config["TEST_NAME"]
  for run in test_runs:
    print """
  
-- starting engines for perftest: nginx-%s
-- GOTO: http://%s:%s/ for a testdrive
  
  """ % (run, test_server, test_port)
  
    os.system("cd %s/%s && ./run_perftest.sh %s" % (cdir, test_env, run))
    os.system("abmeter -t %s -p %s -b 100 -m 1000 -s 100 -n 100000 -u /%s -k -o %s/results" % (test_server, test_port, run, test_env))
    insert_result(tid, run, pname)
  



def insert_result(tid, run, pname):
  print " > inserting result for %s" % pname
  db.init_app(current_app)

  input_file = "testenv/results/perf_data.csv"
  if not os.path.isfile(input_file):
    print "[-] ERROR - cannot find csv_input"
    return(2)
  
  reader = csv.DictReader(open(input_file, "rb")) 
  
  l = PerfLog.query.filter(PerfLog.tid == tid).first()
  if not l:
    l = PerfLog(tid, pname)
    db.session.add(l)
  for row in reader: 
    cc = row["cconnects"]
    r = PerfTest(tid, run, cc, "rps", row["rps"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "time*10ms", row["time*10ms"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "min", row["min"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "max", row["max"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "median", row["median"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "failed", row["failed"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "std_dev", row["std_dev"])
    db.session.add(r)
    r = PerfTest(tid, run, cc, "mean", row["mean"])
    db.session.add(r)
  
  
    print "  > added conns: %s" % cc
  db.session.commit()
  os.remove(input_file)
  #~ rps = row["rps"]
  #~ time = row["time*10ms"]
  #~ tmin = row["min"]
  #~ tmax = row["max"]
  #~ median = row["median"]
  #~ failed = row["failed"]
  #~ std_dev = row["std_dev"]
  #~ mean = row["mean"]


def gen_res():

  from getpass import getuser 
  from time import strftime, localtime, time
  from matplotlib.font_manager import FontProperties
  

  #db.init_app(current_app)
  o_dir = "%s/output" % current_app.config["TESTENV"]
  test_runs = current_app.config["TEST_RUNS"]

  print "> running cleanup-sequence"
  
  empty = db.session.query(PerfTest.tid).filter(PerfTest.rfield == 'rps', PerfTest.res == 0).order_by(PerfTest.tid.desc()).distinct()
  for e in empty:
    print "  > deleting empty set in %s" % e.tid
    db.session.query(PerfTest).filter(PerfTest.tid == e.tid).delete()
    db.session.query(PerfLog).filter(PerfLog.tid == e.tid).delete() 
    db.session.commit()

  plogs = db.session.query(PerfLog.id).all()
  pl = []
  for p in plogs:
    pl.append(p.id)
  notin = db.session.query(PerfTest.tid).filter(PerfTest.tid.in_((pl))).order_by(PerfTest.tid.desc()).distinct()
  for n in notin:
    print " > found orphaned t.id -> %s" % n.tid
    sys.exit()
  

    
  print "> generating charts"
  
  latest = db.session.query(PerfTest.tid, PerfLog.perftest_name).filter(PerfTest.tid == PerfLog.tid).order_by(PerfTest.tid.desc()).first()
  if not latest:
    print "\n\n> nothing found for latest perftest, can i go home now?\n\n"
    return(0)
  ltid = latest.tid
  pname = latest.perftest_name
  latest_fields = db.session.query(PerfTest.rfield).distinct()
  conns_data = db.session.query(PerfTest.conns).filter(PerfTest.tid == latest.tid).order_by(PerfTest.conns).distinct()
  cc = []
  for c in conns_data:
    cc.append(c.conns)
  print "> latest charts: %s" % ltid
  for f in latest_fields:
    rf = f.rfield
    out_file = "%s/latest-%s.png" % (o_dir, rf)
    latest_known = 0
    latest_out = "%s/%s" % (o_dir, latest.tid)
    if not os.path.isdir(latest_out):
      os.mkdir(latest_out)
      f = open("%s/index.html" % latest_out, "w")
      html = test_res_index(pname, latest.tid)
      f.write(html)
      f.close()
    
    print " > latest %s-charts" % rf 
  
    xdata = {}
    # creating latest-rf - figure
    fig = plt.figure()  
    ax = plt.subplot(111)
    for run in test_runs:
      print "    > run : nginx-%s" % run
      data = []
      sdata = db.session.query(PerfTest.res).filter(PerfTest.tid == ltid, PerfTest.rfield == rf, PerfTest.test_name == run).order_by(PerfTest.conns).all()
      mmax = 0
      rc = 0
      for s in sdata:
        data.append((s[0]))
        rc += 1 
        if s[0] > mmax:
          mmax = s[0]
      
      #print len(data)
      #print cc
      
      br = 0
      if len(data) == 0:
        print "    > no data found for %-10s skipping" % rf
        br = 1
      elif len(data) != len(cc):
        print "   [i] skipping %-10s, data-mismatch (data-len != cc-len, [ %s : %s ]) " % (rf, len(data), len(cc))
        br = 1
      if br == 1:
        continue
      
      
      xdata[run] = data
      
      try:
        ax.plot(cc, data, 'o-', label=run, lw=2)
      
      except:
        print " > error while trying to compile data for %s" % rf
        continue
  
    fontP = FontProperties()
    fontP.set_size('small')
    lg = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1, shadow=True)
    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    try:
      frame  = lg.get_frame()
      frame.set_facecolor('0.90')
    except:
      # maybe no axis generated?
      continue 
    # Set the fontsize
    for label in lg.get_texts():
      label.set_fontsize('small')
  
    for label in lg.get_lines():
      label.set_linewidth(1.5)  # the legend line width
    plt.ylabel(rf)
    plt.xlabel("parallel connects")
    plt.ylim(ymin=0)
    plt.title("%s - %s" % (pname, rf))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.draw()
    #plt.show()
    fig.savefig(out_file, dpi=72)
    plt.close()
    cp(out_file, latest_out)
    print " > comparative charts (latest 10)"
    latest_10 = db.session.query(PerfTest.tid, PerfLog.perftest_name).filter(PerfTest.tid == PerfLog.tid).order_by(PerfTest.tid.desc()).limit(10)
  
    for v in test_runs:
      #print "  > processing: %s" % v
      out_file = "%s/zompare-%s-%s.png" % (o_dir, v, rf,)
      fig = plt.figure()
      lsd = [] # min, max, avg
      ax = plt.subplot(111)
      for c in cc:
        lmin = 100000000000000000
        lmax = 0
        lavg = 0
        ltot = 0
        lcnt = 0
        ldata =  db.session.query(PerfTest.res, PerfTest.conns).filter(PerfTest.test_name == v, PerfTest.rfield == rf, PerfTest.conns == c).order_by(PerfTest.tid.desc()).limit(10)
        for l in ldata:
          lr = l.res
          lc = l.conns 
          if lr < lmin:
            lmin = lr
          elif lr > lmax:
            lmax = lr
          try:
            ltot += int(lr)
          except:
            continue
        lavg = int(ltot/ldata.count())
        try:
          lavg = int(ltot/len(ldata))
        except:
          pass 
        lsd.append((lmin, lmax, lavg))
        continue
      tmin = []
      tmax = []
      tavg = []
      for d in lsd:
        tmin.append(d[0])
        tmax.append(d[1])
        tavg.append(d[2])
      try:
        ax.plot(cc, tmin, 'o-', color="r", label='Min', lw=2)
        ax.plot(cc, tmax, 'o-', color="g", label='Max', lw=2)
        ax.plot(cc, tavg, 'o-', color="b", label='Avg', lw=2)
        ax.plot(cc, xdata[v], 'o-', color="m", label='latest-%s' % v, lw=2)
      except:
        print " > error while trying to compile data for %s" % rf
        continue
        
      legend = plt.legend(loc='best')
      #~ # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
      frame  = legend.get_frame()
      frame.set_facecolor('0.90')
    
      # Set the fontsize
      for label in legend.get_texts():
        label.set_fontsize('large')
    
      for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width
      plt.ylabel(rf)
      plt.xlabel("parallel connects")
      plt.ylim(ymin=0)
      plt.title("Comparison - Latest 10 vs - %s | %s" % (pname, rf))
      box = ax.get_position()
      ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
      ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  
      plt.draw()
      #plt.show()
      fig.savefig(out_file, dpi=72)
      plt.close()
  
  print " > latest 50 rps-comparison"
  for c in [100,500,1000]:
    out_file = "%s/zompare-%s.png" % (o_dir, c)
    fig = plt.figure()
    ax = plt.subplot(111)
    for v in test_runs:
      pdata = []
      pc = []
      pcc = 1
      fdata =  db.session.query(PerfTest.res).filter(PerfTest.test_name == v, PerfTest.rfield == "rps", PerfTest.conns == c).order_by(PerfTest.tid.desc()).limit(50)
      for f in fdata:
        pc.append(pcc)
        pcc += 1
        pdata.append(f.res)
      
      ax.plot(pc, pdata, label=v, lw=2, )
    legend = plt.legend(loc='best')
    #~ # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame  = legend.get_frame()
    frame.set_facecolor('0.90')
    
    # Set the fontsize
    for label in legend.get_texts():
      label.set_fontsize('large')
    
    for label in legend.get_lines():
      label.set_linewidth(1.5)  # the legend line width
    plt.ylabel(rf)
    plt.xlabel("Tests Back")
    plt.ylim(ymin=0)
    plt.title("Comparison - Latest RPS / %s Connections" % c)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  
    plt.draw()
    #plt.show()
    fig.savefig(out_file, dpi=72)
    plt.close()
    
  
  perftests = db.session.query(PerfLog.tid, PerfLog.perftest_name).order_by(PerfLog.tid.desc()).limit(100)
  print " > creating html in %s" % o_dir
  
  gen_html(perftests)
  
  print " > check %s/index.html " % o_dir
  
  
  
  #~ 
  #~ # calculating results against older vals


# html_out 

def gen_html(perftest_list):
  o_dir = "%s/output" % current_app.config["TESTENV"]
  test_names = current_app.config["TEST_NAMES"]
  test_runs = current_app.config["TEST_RUNS"]
  html = open("%s/index.html" % o_dir, "w")
  html.write("""
<html>
  <head>
  <title>FroggStack - Perftest-Result</title>
  </head>
<body>
<div align="center">
<h2>FroggStack - Perftest-Results</h2>
<hr>
<a href="latest.html"><img src="latest-rps.png"><br> Latest Results</a>
<a href="zompare.html">Compare All</a> \n
<a href="zompare-runs.html">Compare Runs/CC</a> \n

""")
  c = open("%s/zompare.html" % (o_dir), "w")
  c.write("""<a href="index.html">Index</a><h1>Compare All</h1> \n""")
  for n in test_names:

    for t in test_runs:

      c.write("""<img src="zompare-%s-%s.png">\n""" % (t,n ))
  c.close()

  html.write("""<hr>\n<h2>Perftest-Log (latest 100)</h2><table>""")
  
  for e in perftest_list:
    if len(e.perftest_name) < 2:
      perfname = "run"
    else:
      perfname = e.perftest_name
    html.write("""<tr><td>%s </td><td> <a href="%s/index.html">%s</a> </td></tr>\n""" % (strftime("%F - %H:%M", localtime(float(e.tid))), e.tid, perfname))
  
  html.write("""</table>""")
    
  html.write("""</div></body></html> \n""")
  html.close()
  lres = open("%s/latest.html" % o_dir, "w")
  lres.write("""
<html>
  <head>
  <title>FroggStack - Latest Perftest-Result</title>
  </head>
<body>
<div align="center">
<h2>FroggStack - Latest Perftest-Results</h2>
<hr>
<a href="index.html">Index</a>


""")
  for n in test_names:
    lres.write("""<img src="latest-%s.png"> \n<hr>\n""" % (n))
  lres.write("""</div></body></html> \n""")
  
  lres.close()

  rres = open("%s/zompare-runs.html" % o_dir, "w")
  rres.write("""
<html>
  <head>
  <title>FroggStack - Compare latest RPS </title>
  </head>
<body>
<div align="center">
<h2>FroggStack - Compare latest RPS</h2>
<hr>
<a href="index.html">Index</a>
<hr>
100 Connections <br>
<img src="zompare-100.png">
<hr>
500 Connections <br>
<img src="zompare-500.png">
<hr>
1000 Connections <br>
<img src="zompare-1000.png">
<hr>
</body>
</html>
""")
  rres.close()
  
  
    
def test_res_index(test_name, test_id):
  test_time = strftime("%F - %H:%M", localtime(float(test_id)))
  index_html = """
<html>
  <head>
  <title>FroggStack - Perftest-Result %s</title>
  </head>
<body>
<div align="center">
<h2>FroggStack - Perftest-Results %s - %s</h2>
<hr>
<a href="../index.html"><h2>Index</h2></a>
<hr>

<img src="latest-rps.png"> <hr>
<img src="latest-time*100ms.png"> <hr>
<img src="latest-failed"> <hr>
<img src="latest-mean.png"> <hr>
<img src="latest-median.png"> <hr>
<img src="latest-std_dev.png"> <hr>
<img src="latest-max.png"> <hr>
<img src="latest-min.png"> <hr>
</body>
</html>


  """ % (test_name, test_name, test_time)
  
  
  return(index_html)


