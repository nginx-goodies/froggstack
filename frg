#!/usr/bin/env python
#
#
# nginx-make:
#   - dev
#   - plain
#   - stable
# 

version = "0.3.84 - r57 -  2014-12-10"

import os, sys, time, yaml, csv, getopt
from shutil import copy as cp
from getpass import getuser 
from time import strftime, localtime, time
#import sqlite3 as db

try:
  from flask import Flask, app, session, redirect, url_for, escape, request
except:
  print """[-]

"""
sys.path.append("lib")

# from model import *
from frgstck import * 


def create_app(): 

  # only needed for testenv
  # initiate app
  app = Flask(__name__)
  # load config
  app.config["WORKING_USER"] = getuser()
  # import db and initiate
  from model import db
  db.init_app(app)
  return app




try:
  opts, args = getopt.getopt(sys.argv[1:], "Ehsmlkvdp:c:n:t:T:", 
      ["help", "perftest", "make", "version", "skip-clean", "skip", 
      "keep-branch", "name", "build_template", "list", "exit"  ])
except getopt.GetoptError, err:
  # print help information and exit:
  print " > ERROR on frg / parsing non_existent option " 
  print str(err) # will print something like "option -a not recognized"
  
  sys.exit(2)


cdir = os.getcwd()
x = "help"
mbins = rtest = gres = 0
name = strftime("%F-%H:%M", localtime(time()))
name_set = "0"
skip_clean = "0"
keep_branch = "0"
copts_name = "full"
touch_openssl = "0"
exit_after_opts = "0"

for o, a in opts:

  if o in ('-m', '--make'):
    x = "make"
    
  elif o in ('-p', '--perftest'):
    x = "perftest"
    if a in ("m", "make"):
      mbins = 1
      rtest = 1
      gres = 1
    elif a in ("r", "run"):
      rtest = 1
      gres = 1
    elif a in ('g','genres'):
      gres = 1


    else:
      print "[-] unknown perftest-mode: %s" % a
      x = "help"

  elif o in ("-T"):
    touch_openssl = "%s" % a

  elif o in ("-s", "--skip", "--skip-clean"):
    skip_clean = 1
  elif o in ('-n', '--name'):
    name =  "%s" % a.strip()
    name_set =  "%s" % a.strip()
  elif o in ('-k', '--keep-branch'):
    keep_branch = "1"
  elif o in ('-l', '--list'):
    x = "list"
  elif o in ('-t', '--build_template'):
    copts_name = "%s" % a.strip()

  elif o in ('-E', '--exit'):
    exit_after_opts = "1"
    
  elif o in ('-v', '--version'):
    print """

FROGGSTACK - version %s
    
    """ % version   
    sys.exit()

os.environ["OUT_DIR"] = "builds"
os.environ["TESTENV"] = "testenv"
os.environ["CDIR"] = cdir
os.environ["PYTHONPATH"] = "%s/lib" % cdir
os.environ["VERSION"] = version 
os.environ["NAME_SET"] = name_set
os.environ["SKIP_CLEAN"] = skip_clean
os.environ["KEEP_BRANCH"] = keep_branch
os.environ["COPTS_NAME"] = copts_name
os.environ["TOUCH_OPENSSL"] = touch_openssl
os.environ["EXIT_AFTER_OPTS"] = exit_after_opts

    


if x == "help":
  #app.test_request_context().push()
  frg_help()
  sys.exit()

elif x == "make":
  #app.test_request_context().push()
  ehlo()
  make_bins()
  sys.exit()

elif x == "list":
  #app.test_request_context().push()  
  ehlo()
  print "> available build-templates: (use: -t )\n"
  for c in copts_list():
    print "   > %s " % c
  print "\n\n"
  sys.exit()

elif x == "perftest":

  app = create_app()
  
  app.config["OUT_DIR"] = "builds"
  app.config["TESTENV"] = "testenv"
  app.config["CDIR"] = cdir
  app.config["VERSION"] = version 
  app.config["NAME_SET"] = name_set
  app.config["SKIP_CLEAN"] = skip_clean
  app.config["KEEP_BRANCH"] = keep_branch
  app.config["COPTS_NAME"] = copts_name
  app.config["TOUCH_OPENSSL"] = touch_openssl


  from matplotlib.font_manager import FontProperties
  import matplotlib.pyplot as plt
  import numpy as np

  from perf_test import *


  # todo: make config 
  test_runs = ["plain", "full", "naxsi-full", "naxsi-core", "lua"]
  test_names = "rps failed time*10 min max mean median std_dev".split(" ")
  o_dir = "%s/testenv/output" % cdir
  
  app.config["SQLALCHEMY_DATABASE_URI"]='sqlite:///%s/testenv/output/perfdata.d3b' % cdir
  app.config["TEST_RUNS"] = test_runs
  app.config["TEST_NAME"] = name
  app.config["TEST_NAMES"] = test_names
  # server, port, testenv

  db.init_app(app)
  app.test_request_context().push()


  if mbins == 1:
    make_perftest_bins()
  if rtest == 1:
    run_perftest()
  if gres == 1:
    gen_res()
    

  sys.exit()


else:
  #app.test_request_context().push()
  frg_help()
  sys.exit(1)
