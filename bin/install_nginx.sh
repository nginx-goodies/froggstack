#!/bin/bash
#
# install nginx locally after cloning from repo
#
# 2015-11-05
#


usr=`whoami`

frogg="$HOME/froggstack"

nginx_conf_dir="/etc/nginx"
nginx_bin_src="$frogg/bin/nginx"
nginx_bin_dest="/usr/sbin/nginx"


if [ ! -f /etc/debian_version ]; then

  echo "
[-] ERROR - this works on debian only
  "

fi

echo "

> NGINX_INSTALLATION

"

sudo aptitude -f install liblua5.1-0

echo "
> copying configs
"

sudo mkdir $nginx_conf_dir && sudo chown $usr  $nginx_conf_dir  

if [ ! -z "$etc_repo" ]; then
  git clone "$etc_repo"  etc_nginx
  rsync -avzh $frogg/etc_nginx/*  $nginx_conf_dir/
  
  # f*ck you, systemd
  printf "> is this system systemd'd? [y|N] : "; read hell
  if [ "$hell" = "y" ]; then
    echo "> installing systemd-startup-scripts"
    sudo cp  $nginx_conf_dir/contrib/nginx.service.systemd /etc/systemd/system
    sudo systemctl enable /etc/systemd/system/nginx.service
  else  
    sudo cp $nginx_conf_dir/contrib/nginx.debian.init /etc/init.d/nginx && sudo chmod 755 /etc/init.d/nginx && sudo update-rc.d -f nginx defaults
  fi
fi


echo "
> setting up dhparams
"
# openssl dhparam -out /etc/nginx/dhparams.pem 4096
openssl dhparam -out /etc/nginx/dhparams.pem 2048


echo "
> copying binary
"
sudo cp $nginx_bin_src $nginx_bin_dest



echo "
> copying libjansson
"

sudo cp -a $frogg/contrib/libjansson/* /usr/lib


echo "
> testing config
"

sudo /etc/init.d/nginx test

sudo $nginx_bin_dest -t -c $nginx_conf_dir/nginx.conf

sudo /usr/sbin/update-rc.d -f nginx defaults
