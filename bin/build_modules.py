#!/usr/bin/python
#
#
# 

import os, sys, time, yaml

config = yaml.load("build_config.yaml")

myp = os.getcwd()

modules = "%s/src_modules" % myp

print """

UPDATING nginx-modules

USAGE:
  build_modules.py [sync|build]

"""

if len(sys.argv) < 2:
  sys.exit()

if sys.argv[1] == "build":
  print " > updating modules"
  for mod in os.listdir(modules):
    print "\n> %s" % mod
    mod_dir = "%s/%s" % (modules, mod)
    if os.path.isdir("%s/.git" % mod_dir):
      print "   - trying git"
      os.system("cd %s && git pull && git log > git.log" % mod_dir)
    elif os.path.isdir("%s/.svn" % mod_dir):
      print "   - trying svn"
      os.system("cd %s && svn update && svn log > svn.log" % mod_dir)
    else:
      print "   [i] no vcs found, skipping"
  
else:    
  print " > syncing modules"
  os.system("rsync -avzh --delete  --exclude 'obsolete' --exclude '.libs*' --exclude '.deps*' --exclude  '.openssl*' --exclude  '*.tar.gz' --exclude '.git' --exclude '.svn' --exclude 'openssl-1*' src_modules/* nginx_modules/")


