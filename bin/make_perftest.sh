#!/bin/bash
#
# flynx-nginx
#
# 2014-03-05
#
checkinstall="/usr/bin/checkinstall"

pkgname="froggstack" 
pkgversion="1.4.6" 
pkgrelease="frg-6" 
replaces="nginx,nginx-naxi,nginx-naxi-dv" 
arch="amd64"  
pkglicense="GPL" 
pkggroup="nginx-goodies" 
maintainer="goodman@nginx-goodies.com" 
provides="froggstack"  
pakdir="../"

. venv/bin/activate


skip_build="no"

ngx_versions="plain full naxsi-full naxsi-core lua"


if [ -z $1 ]; then

  echo "USAGE: $0 -x / -s [name]
  -x  - execute full test incl build
  -s  - execute test, skip build 
  "
  exit 

fi

if [ $1 == "-s" ]; then

  skip_build="yes"

fi

mydir=`pwd`
build_dir="$mydir"
tid=`date +%s`
# make and install luajit first
#~ LUAJIT_LIB="/usr/lib"
#~ LUAJIT_INC="/usr/include/luajit-2.0/"
#~ export LUAJIT_LIB
#~ export LUAJIT_INC


nmd="$build_dir/nginx_modules"

nginx_full_opts="
    --conf-path=/etc/nginx/nginx.conf 
    --sbin-path=/usr/sbin/nginx
    --prefix=./
    --error-log-path=/var/log/nginx/error.log
    --http-log-path=/var/log/nginx/access.log
    --http-client-body-temp-path=/var/run/nginx/client_temp
    --http-proxy-temp-path=/var/run/nginx/proxy_temp
    --http-fastcgi-temp-path=/var/run/nginx/fastcgi_temp
    --with-file-aio
    --with-http_gzip_static_module
    --with-http_ssl_module
    --with-http_spdy_module
    --with-http_stub_status_module
    --with-debug
    --without-mail_pop3_module
    --without-mail_smtp_module
    --without-mail_imap_module
    --without-http_uwsgi_module
    --without-http_scgi_module
    --without-http_ssi_module    
    --with-openssl=$nmd/openssl
    --add-module=$nmd/naxsi
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/echo-nginx-module
    --add-module=$nmd/nginx-accesskey
    --add-module=$nmd/ngx_http_log_request_speed
    --add-module=$nmd/set-misc-nginx-module
    --add-module=$nmd/nginx-sticky-module
    --add-module=$nmd/ngx_cache_purge
    --add-module=$nmd/memc-nginx-module
    --add-module=$nmd/lua-nginx-module
    --add-module=$nmd/nginx-upstream-fair
    --add-module=$nmd/headers-more-nginx-module
    

    "

nginx_plain_opts="
    --conf-path=/etc/nginx/nginx.conf 
    --sbin-path=/usr/sbin/nginx
    --prefix=./
    --error-log-path=/var/log/nginx/error.log
    --http-log-path=/var/log/nginx/access.log
    --http-client-body-temp-path=/var/run/nginx/client_temp
    --http-proxy-temp-path=/var/run/nginx/proxy_temp
    --http-fastcgi-temp-path=/var/run/nginx/fastcgi_temp
    --with-file-aio
    --with-http_gzip_static_module
    --with-http_ssl_module
    --with-http_spdy_module
    --with-http_stub_status_module
    --with-debug
    --without-mail_pop3_module
    --without-mail_smtp_module
    --without-mail_imap_module
    --without-http_uwsgi_module
    --without-http_scgi_module
    --without-http_ssi_module    
    "

# configure-docs:
# https://groups.google.com/forum/#!msg/openresty-en/MbegSFArHqg/Hs_aMT3YmdsJ
# openssl / see comments: http://www.stefanwille.com/2013/04/using-spdy-with-nginx-1-4/

# NOT working:  
#    --add-module=$nmd/nginx-limit-upstream
#    --add-module=$nmd/nginx_upstream_check_module
#

#cinstall_opts="-y"

echo "

> FROGGSTACK_PERFTESTS

"

pname=$2
if [ -z "$pname" ]; then
  printf "> test-name [date] : "; read pname
  
fi

PERFTEST_NAME="$pname"
export PERFTEST_NAME


for v in $ngx_versions
  do
  ngx_src="nginx"
  opts="$nginx_full_opts"
  echo "> building nginx: $v"
  
  if [ $v = "plain" ]; then
    opts="$nginx_plain_opts" 
    
  fi

  case $v in
    plain|full)
      if [ "$skip_build" != "yes" ]; then
        skip_build="no"
      fi
      ;;
  esac

    # see http://wiki.nginx.org/HttpLuaModule#Installation
    #export LUA_LIB=/usr/lib/
    #export LUA_INC=/usr/include/lua5.1/
    if [ "$skip_build" = "no" ]; then
      echo "

-- configuring nginx-$v
-- opts: 
$opts

"
    


      cd nginx && ./configure `echo "$opts" |  perl -ne 'chomp and print'` && make 
      ret=$?
      if [ $ret != "0" ]; then
    
        echo "
> configure make not succeed, exiting ... 

"
      exit 2
    
      fi

        echo "
> copying nginx-bins  

"

      sudo killall nginx-$v
      cp $build_dir/nginx/objs/nginx $build_dir/testenv/nginx-$v
      #cp $build_dir/nginx/objs/nginx $build_dir/testenv/nginx-`date +%s`

    
    else
      echo "
> skipping build for perftest
      "
    
    fi
  

  echo "
  
  -- starting engines for perftest: nginx-$v
  
  "
  
  echo "
  -- GOTO: http://127.0.0.1:4202/ for a testdrive
  "
  cd $mydir
  cd $mydir/testenv && sudo ./run_perftest.sh $v
  cd $mydir
  
  abmeter -t 127.0.0.1 -p 4202 -b 100 -m 1000 -s 100 -n 100000 -u /$v -k -o testenv/results
  python perftest.py $v $tid
  

  

done 

echo "
  
  -- creating charts
  
"

python perftest.py genres
