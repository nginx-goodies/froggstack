#!/bin/bash
#
# flynx-nginx
#
# 2014-03-05
#
checkinstall="/usr/bin/checkinstall"

pkgname="froggstack" 
pkgversion="1.4.4" 
pkgrelease="frg-5" 
replaces="nginx,nginx-naxi,nginx-naxi-dv" 
arch="amd64"  
pkglicense="GPL" 
pkggroup="nginx-goodies" 
maintainer="goodman@nginx-goodies.com" 
provides="froggstack"  
pakdir="../"

echo "making froggstack-nginx"


function pre_install() {
    
    # install luajit 2.x from http://luajit.org/download.html
    
    # debian
    sudo aptitude install lua liblua5.2-dev liblua5.2-0 liblua5.2-0-dev libpcre3 libpcre3-dev  liblua5.2-rex-pcre0  liblua5.2-rex-pcre-dev checkinstall
    
    # on debian/ubuntu:
    sudo ln -s /usr/lib/x86_64-linux-gnu/liblua5.2.so /usr/lib/liblua.so
    

    sudo cp -a nginx_modules/lua-resty-memcached/lib/resty/*.* /etc/nginx/lua/

    # sles
    zypper install lua-devel lua liblua5_1 pcre pcre-devel  checkinstall
    
    # redhat/centos
    

    
}

mydir=`pwd`
build_dir="$mydir"

# make and install luajit first
#~ LUAJIT_LIB="/usr/lib"
#~ LUAJIT_INC="/usr/include/luajit-2.0/"
#~ export LUAJIT_LIB
#~ export LUAJIT_INC


cinstall_opts="-y --install=no  --pkgname=$pkgname --pkgversion=$pkgversion --pkgrelease=$pkgrelease --replaces=$replaces -A $arch  --pkglicense=$pkglicense --pkggroup=$pkggroup --maintainer=$maintainer --provides=$provides  --pakdir=$pakdir --deldesc=yes --delspec=yes"

#
# remarks on modules
#
#   - healthcheck_nginx_upstreams -> seems outdated, use nginx_upstream_check_module by yaoweibin
#
#   - openssl -> for compiling on squeeze

nmd="$build_dir/nginx_modules"
nginx_opts="
    --conf-path=/etc/nginx/nginx.conf 
    --sbin-path=/usr/sbin/nginx
    --prefix=./
    --error-log-path=/var/log/nginx/error.log
    --http-log-path=/var/log/nginx/access.log
    --http-client-body-temp-path=/var/run/nginx/client_temp
    --http-proxy-temp-path=/var/run/nginx/proxy_temp
    --http-fastcgi-temp-path=/var/run/nginx/fastcgi_temp
    --with-file-aio
    --with-http_gzip_static_module
    --with-http_ssl_module
    --with-http_spdy_module
    --with-http_stub_status_module
    --with-debug
    --without-mail_pop3_module
    --without-mail_smtp_module
    --without-mail_imap_module
    --without-http_uwsgi_module
    --without-http_scgi_module
    --without-http_ssi_module    
    --with-openssl=$nmd/openssl
    --add-module=$nmd/naxsi
    --add-module=$nmd/ngx_devel_kit
    --add-module=$nmd/echo-nginx-module
    --add-module=$nmd/nginx-accesskey
    --add-module=$nmd/ngx_http_log_request_speed
    --add-module=$nmd/set-misc-nginx-module
    --add-module=$nmd/nginx-sticky-module
    --add-module=$nmd/ngx_cache_purge
    --add-module=$nmd/memc-nginx-module
    --add-module=$nmd/lua-nginx-module
    --add-module=$nmd/nginx-upstream-fair
    --add-module=$nmd/headers-more-nginx-module

# resty preferred, from the ml 2014-03-17
#~ --add-module=../ngx_devel_kit-0.2.19
#~ --add-module=../iconv-nginx-module-0.10
#~ --add-module=../echo-nginx-module-0.51
#~ --add-module=../xss-nginx-module-0.04
#~ --add-module=../ngx_coolkit-0.2rc1
#~ --add-module=../set-misc-nginx-module-0.24
#~ --add-module=../form-input-nginx-module-0.07
#~ --add-module=../encrypted-session-nginx-module-0.03
#~ --add-module=../srcache-nginx-module-0.25
#~ --add-module=../ngx_lua-0.9.5 --add-module=../ngx_lua_upstream-0.01
#~ --add-module=../headers-more-nginx-module-0.25
#~ --add-module=../array-var-nginx-module-0.03
#~ --add-module=../memc-nginx-module-0.14
#~ --add-module=../redis2-nginx-module-0.10
#~ --add-module=../redis-nginx-module-0.3.7
#~ --add-module=../auth-request-nginx-module-0.2
#~ --add-module=../rds-json-nginx-module-0.13
#~ --add-module=../rds-csv-nginx-module-0.05
#~ --with-ld-opt=-Wl,-rpath,/usr/local/openresty/luajit/lib    

    "

nginx_plain_opts="
    --conf-path=/etc/nginx/nginx.conf 
    --sbin-path=/usr/sbin/nginx
    --prefix=./
    --error-log-path=/var/log/nginx/error.log
    --http-log-path=/var/log/nginx/access.log
    --http-client-body-temp-path=/var/run/nginx/client_temp
    --http-proxy-temp-path=/var/run/nginx/proxy_temp
    --http-fastcgi-temp-path=/var/run/nginx/fastcgi_temp
    --with-file-aio
    --with-http_gzip_static_module
    --with-http_ssl_module
    --with-http_spdy_module
    --with-http_stub_status_module
    --with-debug
    --without-mail_pop3_module
    --without-mail_smtp_module
    --without-mail_imap_module
    --without-http_uwsgi_module
    --without-http_scgi_module
    --without-http_ssi_module    
    "

# configure-docs:
# https://groups.google.com/forum/#!msg/openresty-en/MbegSFArHqg/Hs_aMT3YmdsJ
# openssl / see comments: http://www.stefanwille.com/2013/04/using-spdy-with-nginx-1-4/

# NOT working:  
#    --add-module=$nmd/nginx-limit-upstream
#    --add-module=$nmd/nginx_upstream_check_module
#

#cinstall_opts="-y"


if [ -z $1  ]; then

    echo "

-- configuring nginx 
-- opts: 
$nginx_opts

"

    # NO
    #~ echo "
#~ 
#~ > patching nginx-sources first (upstream_modules)
    #~ 
    #~ "
    #~ 
    #~ cd $build_dir/nginx && patch -p1 < ../nginx_modules/nginx_upstream_check_module/check.patch
    #~ cd $build_dir/nginx && patch -p1 < ../nginx_modules/nginx_upstream_check_module/check_1.2.6+.patch
    #~ cd $build_dir/nginx_modules/nginx-upstream-fair && patch -p2 < ../nginx_upstream_check_module/upstream_fair.patch
    #~ cd $build_dir/nginx_modules/nginx-sticky-module && patch -p2 < ../nginx_upstream_check_module/nginx-sticky-module.patch
#~ 
    

    sleep 0.5
    # see http://wiki.nginx.org/HttpLuaModule#Installation
    #export LUA_LIB=/usr/lib/
    #export LUA_INC=/usr/include/lua5.1/
    cd nginx && ./configure `echo "$nginx_plain_opts" |  perl -ne 'chomp and print'` && make 
    ret=$?
    if [ $ret != "0" ]; then
    
        echo "
> configure make not succeed, exiting ... 

"
    exit 2
    
    fi
    

    

else
    echo "

-- skipping configure on request

"
    sleep 1
    install_switch="--install=no"
    #install_switch="--install=yes "
fi


cd $mydir 

echo "fini"


    echo "

-- creating packages

"


echo "sudo checkinstall -R $install_switch $cinstall_opts"




#cd $build_dir/nginx && sudo $checkinstall -D $install_switch $cinstall_opts && sudo $checkinstall -R $install_switch $cinstall_opts
#sudo checkinstall -R $cinstall_opts

cd $mydir && cp $build_dir/nginx/objs/nginx ../nginx-`date +%s`
cd $mydir && cp $build_dir/nginx/objs/nginx ../nginx-latest

sudo killall nginx
cp $build_dir/nginx/objs/nginx $build_dir/testenv/
cp $build_dir/nginx/objs/nginx $build_dir/testenv/nginx-`date +%s`

#~ cd $mydir && cd ../flynx && ../nginx -V && sudo ./run_run.sh
#~ 
#~ cd $mydir
#~ 
#~ #rsync -avzh --exclude .svn nginx_modules/$naxsi_ui doxi-tools/contrib/ 
#~ 
#~ 
    echo "

-- cleaning up

"

#cd $build_dir/nginx && make clean 
#cd $build_dir/nginx_modules/openssl && make clean

echo "
-- GOTO: http://locahost:4201/ for a testdrive
"
cd $mydir/testenv && sudo ./run_run.sh 

echo "
-- Pushing to www.mare-system.de
"

rsync -avzh ../nginx-latest www.mare-system.de:~/
