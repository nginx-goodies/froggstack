#!/usr/bin/env python
#
#
# nginx-make:
#   - dev
#   - plain
#   - stable
# 

import os, sys, time, yaml, csv
from shutil import copy as cp
#import sqlite3 as db
from flask import Flask, app, session, redirect, url_for, escape, request
from getpass import getuser 
from time import strftime, localtime, time
from matplotlib.font_manager import FontProperties

import matplotlib.pyplot as plt
import numpy as np

sys.path.append("lib")

from model import *
from perf_out import gen_html, test_res_index

test_runs = ["plain", "full", "naxsi-full", "naxsi-core", "lua"]
test_names = "rps failed time*10 min max mean median std_dev".split(" ")
skip_inserts = "no"
o_dir = "testenv/output"

print """

PERFTEST

  USAGE:
  
  %s [name] [tid] -> insert new result
  %s genres       -> generate result
  
  name  : full/plain
  tid   : timestamp
  
  """ % (sys.argv[0], sys.argv[0])


if len(sys.argv) < 3:
  if len(sys.argv)< 2:
    sys.exit(2)
  else:
    if sys.argv[1] == "genres":
      skip_inserts = "yes"

else:
  pname =  os.environ["PERFTEST_NAME"]
  if len(pname) == 0:
    pname = strftime("%F %H:%M", localtime(time()))
  name = sys.argv[1]
  tid = sys.argv[2]

def create_app(): 

  print "> app_start"
  # initiate app
  app = Flask(__name__)
  # load config
  app.config["WORKING_USER"] = getuser()
  app.config["SQLALCHEMY_DATABASE_URI"]='sqlite:///testenv/output/perfdata.d3b'
  app.config["OUT_DIR"] = o_dir 
  app.config["TEST_RUNS"] = test_runs
  app.config["TEST_NAMES"] = test_names
  # import db and initiate
  from model import db
  db.init_app(app)
    
  return app





app = create_app()
db.init_app(app)
app.test_request_context().push()


input_file = "testenv/results/perf_data.csv"
reader = csv.DictReader(open(input_file, "rb")) 

if skip_inserts == "no":
  print "> inserting results: %s : %s" % (name, tid)
  l = PerfLog.query.filter(PerfLog.tid == tid).first()
  if not l:
    l = PerfLog(tid, pname)
    db.session.add(l)
  for row in reader: 
    cc = row["cconnects"]
    r = PerfTest(tid, name, cc, "rps", row["rps"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "time*10ms", row["time*10ms"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "min", row["min"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "max", row["max"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "median", row["median"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "failed", row["failed"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "std_dev", row["std_dev"])
    db.session.add(r)
    r = PerfTest(tid, name, cc, "mean", row["mean"])
    db.session.add(r)
  
  
    print "  > added conns: %s" % cc
  db.session.commit()
  #~ rps = row["rps"]
  #~ time = row["time*10ms"]
  #~ tmin = row["min"]
  #~ tmax = row["max"]
  #~ median = row["median"]
  #~ failed = row["failed"]
  #~ std_dev = row["std_dev"]
  #~ mean = row["mean"]

  sys.exit()

print "> running cleanup-sequence"

empty = db.session.query(PerfTest.tid).filter(PerfTest.rfield == 'rps', PerfTest.res == 0).order_by(PerfTest.tid.desc()).distinct()
for e in empty:
  print "  > deleting empty set in %s" % e.tid
  db.session.query(PerfTest).filter(PerfTest.tid == e.tid).delete()
  db.session.query(PerfLog).filter(PerfLog.tid == e.tid).delete() 
  db.session.commit()
  
print "> generating charts"

latest = db.session.query(PerfTest.tid, PerfLog.perftest_name).filter(PerfTest.tid == PerfLog.tid).order_by(PerfTest.tid.desc()).first()
#print latest.tid
if not latest:
  print "\n\n> nothing found for latest perftest, can i go home now?\n\n"
  sys.exit()
pname = latest.perftest_name
latest_fields = db.session.query(PerfTest.rfield).distinct()
conns_data = db.session.query(PerfTest.conns).filter(PerfTest.tid == latest.tid).order_by(PerfTest.conns).distinct()
cc = []
for c in conns_data:
  cc.append(c.conns)
print "> latest charts"
for f in latest_fields:
  rf = f.rfield
  out_file = "%s/latest-%s.png" % (o_dir, rf)
  latest_known = 0
  latest_out = "%s/%s" % (o_dir, latest.tid)
  if not os.path.isdir(latest_out):
    os.mkdir(latest_out)
    f = open("%s/index.html" % latest_out, "w")
    html = test_res_index(pname, latest.tid)
    f.write(html)
    f.close()
  
  print " > latest %s-charts" % rf 

  xdata = {}
  # creating latest-rf - figure
  fig = plt.figure()  
  ax = plt.subplot(111)
  for run in test_runs:
    #print "    > run : %s" % run
    data = []
    sdata = db.session.query(PerfTest.res).filter(PerfTest.tid == latest.tid, PerfTest.rfield == rf, PerfTest.test_name == run).order_by(PerfTest.conns).all()
    mmax = 0
    rc = 0
    for s in sdata:
      data.append((s[0]))
      rc += 1 
      if s[0] > mmax:
        mmax = s[0]
    
    #print len(data)
    #print cc
    
    br = 0
    if len(data) == 0:
      print " > no data found for %-10s skipping" % rf
      br = 1
    elif len(data) != len(cc):
      print "  [i] skipping %-10s, data-mismatch (data-len != cc-len, [ %s : %s ]) " % (rf, len(data), len(cc))
      br = 1
    if br == 1:
      continue
    
    
    xdata[run] = data
    
    try:
      ax.plot(cc, data, 'o-', label=run, lw=2)
    
    except:
      print " > error while trying to compile data for %s" % rf
      continue

  fontP = FontProperties()
  fontP.set_size('small')
  legend = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1, shadow=True)
  # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
  frame  = legend.get_frame()
  frame.set_facecolor('0.90')

  # Set the fontsize
  for label in legend.get_texts():
    label.set_fontsize('small')

  for label in legend.get_lines():
    label.set_linewidth(1.5)  # the legend line width
  plt.ylabel(rf)
  plt.xlabel("parallel connects")
  plt.ylim(ymin=0)
  plt.title("%s - %s" % (pname, rf))
  box = ax.get_position()
  ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
  ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  plt.draw()
  plt.show()
  fig.savefig(out_file, dpi=72)
  plt.close()
  cp(out_file, latest_out)
  print " > comparative charts (latest 10)"
  latest_10 = db.session.query(PerfTest.tid, PerfLog.perftest_name).filter(PerfTest.tid == PerfLog.tid).order_by(PerfTest.tid.desc()).limit(10)

  for v in test_runs:
    #print "  > processing: %s" % v
    out_file = "%s/zompare-%s-%s.png" % (o_dir, v, rf,)
    fig = plt.figure()
    lsd = [] # min, max, avg
    ax = plt.subplot(111)
    for c in cc:
      lmin = 100000000000000000
      lmax = 0
      lavg = 0
      ltot = 0
      lcnt = 0
      ldata =  db.session.query(PerfTest.res, PerfTest.conns).filter(PerfTest.test_name == v, PerfTest.rfield == rf, PerfTest.conns == c).order_by(PerfTest.tid.desc()).limit(10)
      for l in ldata:
        lr = l.res
        lc = l.conns 
        if lr < lmin:
          lmin = lr
        elif lr > lmax:
          lmax = lr
        try:
          ltot += int(lr)
        except:
          continue
      lavg = int(ltot/ldata.count())
      try:
        lavg = int(ltot/len(ldata))
      except:
        pass 
      lsd.append((lmin, lmax, lavg))
      continue
    tmin = []
    tmax = []
    tavg = []
    for d in lsd:
      tmin.append(d[0])
      tmax.append(d[1])
      tavg.append(d[2])
    try:
      ax.plot(cc, tmin, 'o-', color="r", label='Min', lw=2)
      ax.plot(cc, tmax, 'o-', color="g", label='Max', lw=2)
      ax.plot(cc, tavg, 'o-', color="b", label='Avg', lw=2)
      ax.plot(cc, xdata[v], 'o-', color="m", label='latest-%s' % v, lw=2)
    except:
      print " > error while trying to compile data for %s" % rf
      continue
      
    legend = plt.legend(loc='best')
    #~ # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame  = legend.get_frame()
    frame.set_facecolor('0.90')
  
    # Set the fontsize
    for label in legend.get_texts():
      label.set_fontsize('large')
  
    for label in legend.get_lines():
      label.set_linewidth(1.5)  # the legend line width
    plt.ylabel(rf)
    plt.xlabel("parallel connects")
    plt.ylim(ymin=0)
    plt.title("Comparison - Latest 10 vs - %s | %s" % (pname, rf))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    plt.draw()
    plt.show()
    fig.savefig(out_file, dpi=72)
    plt.close()

print " > latest 50 rps-comparison"
for c in [100,500,1000]:
  out_file = "%s/zompare-%s.png" % (o_dir, c)
  fig = plt.figure()
  ax = plt.subplot(111)
  for v in test_runs:
    pdata = []
    pc = []
    pcc = 1
    fdata =  db.session.query(PerfTest.res).filter(PerfTest.test_name == v, PerfTest.rfield == "rps", PerfTest.conns == c).order_by(PerfTest.tid.desc()).limit(50)
    for f in fdata:
      pc.append(pcc)
      pcc += 1
      pdata.append(f.res)
    
    ax.plot(pc, pdata, label=v, lw=2, )
  legend = plt.legend(loc='best')
  #~ # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
  frame  = legend.get_frame()
  frame.set_facecolor('0.90')
  
  # Set the fontsize
  for label in legend.get_texts():
    label.set_fontsize('large')
  
  for label in legend.get_lines():
    label.set_linewidth(1.5)  # the legend line width
  plt.ylabel(rf)
  plt.xlabel("Tests Back")
  plt.ylim(ymin=0)
  plt.title("Comparison - Latest RPS / %s Connections" % c)
  box = ax.get_position()
  ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
  ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

  plt.draw()
  plt.show()
  fig.savefig(out_file, dpi=72)
  plt.close()
  

perftests = db.session.query(PerfLog.tid, PerfLog.perftest_name).order_by(PerfLog.tid.desc()).limit(100)
print " > creating html in %s" % o_dir

gen_html(perftests)

print " > check %s/index.html " % o_dir



#~ 
#~ # calculating results against older vals
